package hermes.web;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import hermes.dominio.Producto;
import hermes.logica.ProductService;


@RestController
@RequestMapping("/producto")
@CrossOrigin(origins = "*")
public class ProductController {

	@Autowired
	private ProductService _service;
	
	// CALL CONSULTAS
	@GetMapping
	public List<Producto> getAllPrductos(){
		return _service.listarTodosProductosHabilitados();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Producto getPrducto(@PathVariable(name = "id") Integer id){
		try {
			return _service.obtenerProducto(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = "/listar/{idTipo}", method = RequestMethod.GET)
	public List<Producto> getPrductosPorTipoProducto(@PathVariable(name = "idTipo") Integer idTipo){
		try {
			return _service.getPrductosPorTipoProducto(idTipo);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
