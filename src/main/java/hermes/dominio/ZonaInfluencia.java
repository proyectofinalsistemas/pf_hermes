package hermes.dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity 
public class ZonaInfluencia {
	
	@Id @GeneratedValue
	private Integer id;
	@OneToMany(fetch = FetchType.EAGER)
	@NotNull @NonNull
	private List<PuntoGeografico> puntos; 		//Los puntos que al unir, formar�n la zona de inluencia de la sucursal.
	@NotNull @NonNull
	private String descripcion;
	
	public ZonaInfluencia(MapModel map, String sucursal){
		setPuntos(createPuntoGeografico(map));
		setDescripcion("Zona"+ sucursal);
	}
	
	private List<PuntoGeografico> createPuntoGeografico(MapModel map) {
		PuntoGeografico pto;
		List<PuntoGeografico> puntos = new ArrayList<>();
		//puntos=null;
		for (Marker mark : map.getMarkers()){
			pto = new PuntoGeografico(mark.getLatlng());
			puntos.add(pto);
		}
		return puntos;	
	}
	
}
