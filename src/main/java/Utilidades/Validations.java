package Utilidades;

import java.util.ArrayList;
import java.util.List;

public class Validations extends Exception {

    public List<Mensaje> mensajes = new ArrayList<Mensaje>();

    public List<Mensaje> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<Mensaje> mensajes) {
        this.mensajes = mensajes;
    } 
    
    public boolean isValid() {
        return mensajes.size()==0;
    }

    public String toString(){
        return ("Se ha producido la excepci�n: " + "\n" + this.getClass().getName() + "\n" + "Con el siguiente mensaje: " + this.getMessage() + "\n");
    }
}
