package hermes.dominio;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity 
public class EstadoPedido {
	@Id @NotNull
	private @NonNull Integer id;
	@NotNull
	private @NonNull String nombre;
	private String descripcion;
	
	@Override
	public String toString(){
		return nombre;
	}
}
