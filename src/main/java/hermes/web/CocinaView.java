package hermes.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

import hermes.dominio.DetallePedido;
import hermes.dominio.EstadoPedido;
import hermes.dominio.Pedido;
import hermes.dominio.Producto;
import hermes.logica.OrderService;
import lombok.Getter;
import lombok.Setter;

@RestController
@Scope("view")
public class CocinaView {
	@Autowired
	private OrderService servicio;
	private List<Pedido> pedidosCocina;
	private @Setter @Getter DetallePedido detalleSeleccionado;
	private @Setter @Getter Pedido pedidoSeleccionado;
	private @Setter @Getter boolean sinCambios;
	private @Setter @Getter Pedido pedidoADeshacer;
	private @Setter @Getter DetallePedido detalleADeshacer;
	private @Setter @Getter EstadoPedido estadoADeshacer;
	
	@PostConstruct
	public void init(){
		pedidosCocina = new ArrayList<>();
		pedidosCocina.clear();
		pedidosCocina.addAll(servicio.pedidosCocina());
		ordenarPedidos();
		setSinCambios(true);							//Variable utilizada para permitir o no la funcionalidad "Deshacer" en la vista.
	}

	public void ordenarPedidos() {						//Ordena los pedidos a ser mostrados en cocina de modo ascendente seg�n la hora en que se realizaron. 
		Collections.sort(pedidosCocina,comparator);
	}

	public List<Pedido> getPedidosCocina() {
		return pedidosCocina;
	}

	public void setPedidosCocina(List<Pedido> pedidosCocina) {
		this.pedidosCocina = pedidosCocina;
	}
	
	Comparator<Pedido> comparator = new Comparator<Pedido>() {
	    public int compare(Pedido a, Pedido b) {
	    	int resultado = a.getHoraPedido().compareTo(b.getHoraPedido());
	        return resultado;
	    }
	};
	
	public void rowDblSelect() {							//M�todo que captura el evento Doble Click. �ste m�todo se utiliza para la actualizaci�n del estado de un detalle y/o pedido al cual se le aplic� el doble click.
		for(Pedido pedido : pedidosCocina){
			for(DetallePedido detalle : pedido.getDetalle()){
				if(detalleSeleccionado.getId().equals(detalle.getId())){
					setPedidoSeleccionado(pedido);
				}
			}
		}
		setADeshacer();										//Seteo de variables necesarias, para ofrecer la funcionalidad de "Deshacer".
		cambiarEstado();									//Funcionalidad de este m�todo, realizar un cambio de estado del detalle seleccionado.
		setSinCambios(false);								//Se setea la variable "Sin Cambios" a modo de indicar que la funcionalidad "Deshacer" se encuentra disponible para ser utilizada.
	}
	
	public void setADeshacer() { 							//M�todo de seteo de variables necesarias para la funcionalidad "Deshacer". Se podr�a cambiar para permitir deshacer m�s de una �nica acci�n.
		setPedidoADeshacer(pedidoSeleccionado);				//Pedido involucrado en la acci�n a volver atras.
		setDetalleADeshacer(detalleSeleccionado);			//Detalle pedido al que se volver� una acci�n atras. 
		setEstadoADeshacer(detalleADeshacer.getEstado());	//Estado al que se devolver� el detalle definido arriba.
	}

	private void cambiarEstado() {
		Integer devolucion = servicio.cambiarEstado(pedidoSeleccionado, detalleSeleccionado);
		if(devolucion.equals(0)){
			addMessage("Cambio no realizado", "Elemento ya preparado");
		}else if(devolucion.equals(2)){
			pedidosCocina.remove(pedidoSeleccionado);
			pedidoSeleccionado=null;
			addMessage("Pedido realizado con �xito.", "Pedido Completado");
		}		
	}
	
	public void deshacer(){								//M�todo el cu�l le permite a los usuarios de Cocina volver una acci�n atras. Se podr�a ver de ampliar el nro de acciones permitidas.
		Pedido pedido = servicio.deshacerAccion(getPedidoADeshacer(), getDetalleADeshacer(), getEstadoADeshacer()); //Delego la tarea al Servicio, encargado de la l�gica de negocio.
		if(pedidosCocina.isEmpty()){					//En caso de que la lista de pedidos en cocina est� vac�a, s�lo queda agregar el pedido al que le deshizo la acci�n, sin ninguna validaci�n extra.
			pedidosCocina.add(pedido);
		}else{											//En caso que dicha lista no est� vac�a, se debe verificar si dicho pedido a�n se encuentra en la lista, para reemplazarlo, o si s�lo resta volver a agregarlo.
			int index = -1;
			Pedido ped=null;
			for(Pedido p : pedidosCocina){				
				if((p.getId()).equals(pedido.getId())){ //Busco dentro de la lista de pedidos de cocina actual si est� el pedido al que se le deshizoo la acci�n, para reemplazarlo.
					index = pedidosCocina.indexOf(p);	//En caso de encontrar el pedido a reemplazar (al deshacer la acci�n, el pedido queda desactualizado y debe ser reemplazado), se obtiene su index dentro de la lista.
					ped=p;								//Guardo el pedido a reemplazar, para luego quitarlo. 
				}
			}
			if(index>-1){								//Siempre que haya encontrado el pedido a reemplazar dentro de la lista, la variable index va a ser mayor que -1 (valor por defecto al no encontrar un elemento dentro de la lista). Por lo tanto estoy verificando si encontr� o no el elemento.
				pedidosCocina.remove(ped);				//Quito el elemento desactualizado de la lista.
				pedidosCocina.add(index, pedido);		//Agrego el elemento actualizado en el mismo lugar en que estaba el anterior (mismo elemento, pero actualizado).
			}else{										//En caso que la variable index valga -1, indica que no se encuentra el elemento desactualizado dentro de la lista, por lo tanto s�lo queda agregar el mismo a dicha lista.
				pedidosCocina.add(pedido);				
				ordenarPedidos();						//Una vez agregado el elemento, en la l�nea anterior, se procede a ordenar nuevamente la lista, a modo que en cocina se siga apreciando los pedidos ordenados seg�n el mismo criterio de cuando se carga la vista.
			}
		}
		setSinCambios(true);							//Se setea la variable "Sin Cambios" a True, de modo que el bot�n "Deshacer" quedar� inactivo. En caso de modificar este m�todo permitiendo volver atras a m�s acciones, �sta variable deber�a verificar que no contiene m�s acciones posibles a deshacer.
	}

	public boolean esCancelado(String estado){
		if(estado.equals("Cancelado")){
			return false;
		} else{
			return true;
		}
	}
	
	public void addMessage(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void addMessageError(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
}
