package hermes.persistencia;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.Cancelacion;

@Repository
public interface CancellationRepository extends PagingAndSortingRepository<Cancelacion, Integer> {

}
