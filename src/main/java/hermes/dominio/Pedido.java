	package hermes.dominio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ColumnDefault;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity
public class Pedido{

	@Id @GeneratedValue @NotNull
	private Long id;
	@NotNull @Temporal(TemporalType.DATE)
	private @NonNull Date fechaPedido;
	@NotNull @Temporal(TemporalType.TIME)
	private @NonNull Date horaPedido;
	@Temporal(TemporalType.DATE)
	private Date fechaEntrega;
	@Temporal(TemporalType.TIME)
	private Date horaEntrega;
	@OneToMany(fetch=FetchType.EAGER) @NotNull
	private @NonNull List<DetallePedido> detalle;
	@Min(0) @NotNull
	private @NonNull BigDecimal total;
	@ManyToOne @NotNull
	private @NonNull EstadoPedido estado;
	@ManyToOne @NotNull
	private @NonNull Sucursal sucursal;
	@ManyToOne @NotNull
	private @NonNull TipoPago tipoPago;
	@ManyToOne @NotNull
	private @NonNull TipoEntrega tipoEntrega;
	private String comentarios;
	//En caso de que el Tipo de Entrega sea "Env�o a Domicilio", el pedido necesita tener coordenadas para que se realice la l�gica de delivery.
	@ManyToOne
	private PuntoGeografico coordenadas;	
	private String calle;
	private int numero;
	private String dpto;
	private String piso;

	// CONSTRUCTORES
	/* El contructor por defecto es para un pedido de preparaci�n lo m�s pronto posible, con retiro en sucursal. */
	/* Solicita el pedido con retiro en sucursal. */ 
	public Pedido(Date fechaPedido, Date hsPedido, Date fechaEntrega, Date hsEntrega, List<DetallePedido> detalles, 
					BigDecimal total, EstadoPedido estado, Sucursal sucursal, TipoPago pago, TipoEntrega entrega, String comentarios) {
		this.fechaPedido = fechaPedido;
		this.horaPedido = hsPedido;
		this.fechaEntrega = fechaEntrega;
		this.horaEntrega = hsEntrega;
		this.detalle = crearDetalles(detalles);
		this.total = total;
		this.estado = estado;
		this.sucursal = sucursal;
		this.tipoPago = pago;
		this.tipoEntrega = entrega;
		this.comentarios = comentarios;
	}
	
	private List<DetallePedido> crearDetalles(List<DetallePedido> detalles) {
		List<DetallePedido> detallesAPersistir = new ArrayList<DetallePedido>();
		detalles.stream()
					.forEach(d -> detallesAPersistir.add(new DetallePedido(d.getProducto(), d.getSubtotal(), d.getCantidad(), d.getEstado(), 
														d.getNombreProducto(), d.getNombreTipoProducto(), d.getPrecioProducto())));
		return detallesAPersistir;
	}

	/* Solicita el pedido con env�o a domicilio. */
	public Pedido(Date fechaPedido, Date hsPedido, Date fechaEntrega, Date hsEntrega, List<DetallePedido> detalles, BigDecimal total, EstadoPedido estado,
					Sucursal sucursal, TipoPago pago, TipoEntrega entrega, PuntoGeografico coord, String calle, int nro, String dpto, String piso, String comentarios) {
		this.fechaPedido = fechaPedido;
		this.horaPedido = hsPedido;
		this.fechaEntrega = fechaEntrega;
		this.horaEntrega = hsEntrega;
		this.detalle = crearDetalles(detalles);
		this.total = total;
		this.estado = estado;
		this.sucursal = sucursal;
		this.tipoPago = pago;
		this.tipoEntrega = entrega;
		this.coordenadas = coord;
		this.calle = calle;
		this.numero = nro;
		this.piso = piso;
		this.dpto = dpto;
		this.comentarios = comentarios;
	}
	
	// SETTERS AND GETTERS
	public void setEstado(EstadoPedido estado){
		this.estado = estado;
	}
}
