package Utilidades;

import lombok.Data;

@Data
public class Mensaje {

	private String estado;
	private String descripcion;
	
	public Mensaje(String estado, String descripcion) {
		this.estado = estado;
		this.descripcion = descripcion;
	}
}
