package hermes.logica;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hermes.dominio.PuntoGeografico;
import hermes.dominio.ZonaInfluencia;
import hermes.persistencia.GeoPointRepository;
import hermes.persistencia.InfluenceZoneRepository;
import hermes.web.GeocodeView;
import lombok.Getter;
import lombok.Setter;

@Service
public class GeolocationService {

	@Autowired
	private GeoPointRepository repositorioPto;
	@Autowired 
	private InfluenceZoneRepository repositorioZona;
	private @Setter @Getter GeocodeView geocode;
	private @Setter @Getter List<PuntoGeografico> puntos;
	private @Setter @Getter MapModel mapa;
	private @Setter @Getter boolean hayZona;
	
//	public boolean hayZona() {
//		return this.hayZona;
//	}

	public MapModel obtenerGeoModel() {
		//MapModel mapa =geocode.getGeoModel();
		return getMapa();
	}

	public LatLng getSucursal() {
		return mapa.getMarkers().get(0).getLatlng();
		//return geocode.getSucursal();
	}

	public void limpiarMapa() {
		geocode.limpiarMapa();
	}

	public void geocode() {
		geocode = new GeocodeView();
		geocode.setAgregarPto(true);
		geocode.setConfirmarPto(true);
	}

	public List<PuntoGeografico> validarPtoSucursal() {
		return validarPto(geocode.getSucursal());
	}

	public List<PuntoGeografico> validarPtoSucursal(LatLng coord) {
		return validarPto(coord);
	}
	
	private List<PuntoGeografico> validarPto(LatLng punto) {
		PuntoGeografico puntoG = new PuntoGeografico(punto);
		setPuntos(repositorioPto.findByPunto(puntoG.getPunto()));
		return getPuntos();
	}
																				
	public void guardarPuntoGeografico(List<PuntoGeografico> puntos) {			//TODO Definir.
		List<PuntoGeografico> pto = new ArrayList<>();							//Formas de validar que un punto no se encuentre ya guardado en BDD:		
		for(PuntoGeografico punto: puntos){										//1-Traer TODOS los puntos de la BDD y hacer un doble for, para recorrer punto por punto de la zona, respecto de los ya guardados.
			pto = repositorioPto.findByPunto(punto.getPunto());					//2-Preguntar por cada punto, directamente a la BDD si ya existe uno igual.
			if(pto.isEmpty()){													//El 1 parece m�s costoso en tiempo de procesamieto, pero s�lo 1 consulta a la BDD (para traer todos los puntos almacenados).
				actualizarPuntoGeografico(punto);								//El 2 parece menos costoso en procesamiento, pero hace 1 consulta a la BDD por cada punto de la zona de influencia.
			}																	//Us� moment�neamente la opci�n 2, solo por comodidad y menor cantidad de l�neas de c�digo. Adem�s que se espera que no sea una funcionalidad habitual.	
		}																																								
	}
	
	public void guardarZonaInfluencia(ZonaInfluencia zona) {
		actualizarZonaInfluencia(zona);
	}
	
	private void actualizarZonaInfluencia(ZonaInfluencia zona) {
		repositorioZona.save(zona);	
	}
	
	public void guardarPuntoGeografico(PuntoGeografico geolocalizacion) {
		actualizarPuntoGeografico(geolocalizacion);		
	}

	private void actualizarPuntoGeografico(PuntoGeografico punto) {
		repositorioPto.save(punto);
	}

	public void setMapa(MapModel geoModel) {
		this.mapa=geoModel;
	}

}
