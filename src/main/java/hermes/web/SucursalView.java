package hermes.web;

import hermes.dominio.Ciudad;
import hermes.dominio.Pedido;
import hermes.dominio.Provincia;
import hermes.dominio.Sucursal;
import hermes.logica.BranchService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javassist.expr.Cast;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import lombok.Getter;
import lombok.Setter;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@SessionScoped
@Scope("view")
public class SucursalView implements Serializable {
	@Autowired
	private BranchService servicio;
	private List<Sucursal> sucursales;
	private @Getter @Setter List<Ciudad> ciudades;
	private List<Provincia> provincias;
	private List<Sucursal> filtroSucursal;
	private Sucursal sucursalSeleccionada;
	private String nombre;
	private String telefono;
	private String calle;
	private int numero;
	private Ciudad ciudad;
	private Provincia provincia;
	private String descripcion;
	private String provinciaSeleccionada;
	private String ciudadSeleccionada;
	private boolean noHaySeleccionado;
	private boolean esHabilitada;
	private @Getter @Setter List<Pedido> pedidosAfectados;
	private boolean hayDireccion;
	private @Getter @Setter boolean modoSucursal;

	@PostConstruct
	public void init() {
		this.cargarSucursal();
		this.cargarProvincias();
		this.cargarCiudades();
		this.noHaySeleccionado = true;
		setCalle("");
	}
	public void cargarSucursal(){
		sucursales = servicio.listarTodasSucursales();
	}
	
	public void cargarProvincias() {
		provincias = servicio.listarTodasProvincias();
	}

	public void cargarCiudades() {
		ciudades = servicio.listarTodasCiudades();
	}

	public void save() {
		ciudad = servicio.obtenerCiudad(ciudadSeleccionada);
		if (this.validarSucursalNoExistente(ciudad)) {	
			servicio.guardarSucursal(nombre, telefono, calle, numero, descripcion, ciudad, esHabilitada);
			addMessage("Guardado con éxito", "Datos guardados");
			cargarSucursal();
			cerrarDialogo("dlgNuevaSucursal");
		} else { 
			 addMessage("Sucursal ya existente","");
		}
		deshabilitarBotones();
	}
	
	public void guardar() {
		ciudad = servicio.obtenerCiudad(ciudadSeleccionada);
		if (this.validarNombreNoExistente()) {
			if (this.validarDireccionNoExistente(ciudad)) {
				servicio.guardarSucursal(nombre, telefono, calle, numero, descripcion, ciudad, esHabilitada);
				addMessage("Guardado con éxito", "Datos guardados");
				cargarSucursal();
				cerrarDialogo("dlgNuevaSucursal");
			} else {
				addMessage("Dirección ya existente","");
			}
		} else {
			addMessage("El nombre ya está en uso", "");
		}
		cargarSucursal();
		deshabilitarBotones();
	}
	
	public boolean validarNombreNoExistente() {
		boolean noExiste;
		noExiste = servicio.nombreNoExistente(nombre);
		return noExiste;
	}
	
	public boolean validarDireccionNoExistente(Ciudad ciudad){
		boolean noExiste;
		noExiste = servicio.direccionNoExistente(calle, numero, ciudad);
		return noExiste;
	}
	
	public void addMessage(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	public void borrarVista() {
		nombre = "";
		telefono = "";
		calle = "";
		descripcion = "";
		numero = 0;
		esHabilitada = true;
		ciudadSeleccionada = "Villa María";
		provinciaSeleccionada = "Córdoba";
		filtrarCiudades();
		modoNvaSucursal();
	}
	
	private void modoNvaSucursal() {
		servicio.modoNvaSucursal();	
	}
	public void limpiarMapa() {
		servicio.limpiarMapa();
	}

	public void validarModificacion() {
		if((sucursalSeleccionada.getNombre().equals(nombre))&&(sucursalSeleccionada.getCalle().equals(calle))&&(sucursalSeleccionada.getCiudad().getNombre().equals(ciudadSeleccionada))&&(sucursalSeleccionada.getDescripcion().equals(descripcion))&&
					(sucursalSeleccionada.getTelefono().equals(telefono))&&(sucursalSeleccionada.getNumero()==numero)&&(sucursalSeleccionada.getHabilitado()==esHabilitada)) {
			addMessage("No Guardado", "No modificó ningún dato.");
		}
		else{
			boolean verificarSucursal = servicio.verificarModificacionSucursal(sucursalSeleccionada);
			if (verificarSucursal) {
				setPedidosAfectados(servicio.verificarSucursalDeshabilitada(sucursalSeleccionada));
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlgVerificacionModificacion').show()");
			} else {
				updateSucursal();
			}
		}
	}
	
	public void updateSucursal() {
		if (this.validarSucursalNoExistente(sucursalSeleccionada)) {	//pasar param sucursalSeleccionada
		ciudad = servicio.obtenerCiudad(ciudadSeleccionada);
		provincia = servicio.obtenerProvincia(provinciaSeleccionada);
		sucursalSeleccionada.setNombre(nombre);
		sucursalSeleccionada.setTelefono(telefono);
		sucursalSeleccionada.setCalle(calle);
		sucursalSeleccionada.setNumero(numero);
		sucursalSeleccionada.setCiudad(ciudad);
		sucursalSeleccionada.setDescripcion(descripcion);
		sucursalSeleccionada.setHabilitado(esHabilitada);
		servicio.actualizarSucursal(sucursalSeleccionada);
		addMessage("Guardado con éxito", "Datos guardados");
 		deshabilitarBotones();
		cerrarDialogo("dlgModificarSucursal");
		}
		/*else {
			if (this.validarNombreNoExistente()) {
				if (this.validarDireccionNoExistente(ciudad)) {
				} else {
					addMessage("Dirección ya existente","");
				}
			} else {
				addMessage("El nombre ya está en uso", "");
			}
		}*/
		else { 
			 addMessage("Sucursal ya existente","");
		}
	}
	
	public boolean validarSucursalNoExistente(Ciudad ciudad) {
		boolean noExiste; 
		noExiste = servicio.sucursalNoExistente(nombre, calle, numero, ciudad);
		return noExiste;
	}

	public boolean validarSucursalNoExistente(Sucursal sucursalSeleccionada) {
		boolean noExiste; 
		noExiste = servicio.sucursalNoExistente(nombre, calle, numero, ciudadSeleccionada, sucursalSeleccionada);
		return noExiste;
	}

	public void modificarSucursal() {
		setNombre(sucursalSeleccionada.getNombre());
		setTelefono(sucursalSeleccionada.getTelefono());
		setCalle(sucursalSeleccionada.getCalle());
		setNumero(sucursalSeleccionada.getNumero());
		setDescripcion(sucursalSeleccionada.getDescripcion());
		setEsHabilitada(sucursalSeleccionada.getHabilitado());
		setProvinciaSeleccionada(sucursalSeleccionada.getCiudad().getProvincia().getNombre());
		filtrarCiudades();
		setCiudadSeleccionada(sucursalSeleccionada.getCiudad().getNombre());
	}
	
	public void verificarDeshabilitacion() {
		if (sucursalSeleccionada.getHabilitado()) {
			boolean verificar = servicio.verificarModificacionSucursal(sucursalSeleccionada);
			if (verificar) {
				setPedidosAfectados(servicio.verificarSucursalDeshabilitada(sucursalSeleccionada));
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlgVerificarDeshabilitacion').show()");
			} else {
				cambiarEstado();
			}
		} else {
			cambiarEstado();
		}
	}
	
	public void cambiarEstado() {
		servicio.cambiarEstado(sucursalSeleccionada);
		deshabilitarBotones();
		addMessage("Datos guardados", "Estado cambiado");
		
	}

	public void onRowSelect(SelectEvent event) {
	        this.noHaySeleccionado=false;
	}

	public void rowDblSelectSucursal(){
	    	modificarSucursal();
	    	RequestContext context = RequestContext.getCurrentInstance();
	    	context.execute("PF('dlgModificarSucursal').show()");
	}

	public void deshabilitarBotones(){
	    	this.setNoHaySeleccionado(true);
	    	this.setSucursalSeleccionada(null);
	}
	
	public void cerrarDialogo(String dialog){
    	RequestContext context = RequestContext.getCurrentInstance();
    	context.execute("PF('"+dialog+"').hide()");
    }
	
	public boolean getHayDireccion(){
		setHayDireccion();
		return this.hayDireccion;
	}
	
	public void setHayDireccion(){
		if (calle.equals(""))  { 					//|| (!isModoSucursal()))  -->No se por qu� lo hab�a agregado (NR)
			this.hayDireccion = true;
		} else {
			this.hayDireccion = false;
		}
	}

//	public void setPedidosAfectados(Set<Pedido> pedidos){
//		this.pedidosAfectados.addAll(pedidos);
//	}
	
	public void filtrarCiudades() {
            ciudades = servicio.listarCiudadesPorProvincia(provinciaSeleccionada);
    }
	
	public List<Sucursal> getSucursales() {
		return sucursales;
	}

	public void setSucursales(List<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	public Sucursal getSucursalSeleccionada() {
		return sucursalSeleccionada;
	}
	
	public void setSucursalSeleccionada(Sucursal sucursalSeleccionada) {
		this.sucursalSeleccionada = sucursalSeleccionada;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}
	
	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	public boolean isEsHabilitada() {
		return esHabilitada;
	}

	public void setEsHabilitada(boolean esHabilitado) {
			this.esHabilitada = esHabilitado;		
	}

	public boolean isNoHaySeleccionado() {
		return noHaySeleccionado;
	}

	public void setNoHaySeleccionado(boolean noHaySeleccionado) {
		this.noHaySeleccionado = noHaySeleccionado;
	}

	public String getCiudadSeleccionada() {
		return ciudadSeleccionada;
	}

	public void setCiudadSeleccionada(String ciudadSeleccionada) {
		this.ciudadSeleccionada = ciudadSeleccionada;
	}

	public List<Sucursal> getFiltroSucursal() {
		return filtroSucursal;
	}

	public void setFiltroSucursal(List<Sucursal> filtroSucursal) {
		this.filtroSucursal = filtroSucursal;
	}

	public List<Provincia> getProvincias() {
		return provincias;
	}

	public void setProvincias(List<Provincia> provincias) {
		this.provincias = provincias;
	}

	public String getProvinciaSeleccionada() {
		return provinciaSeleccionada;
	}

	public void setProvinciaSeleccionada(String provinciaSeleccionada) {
		this.provinciaSeleccionada = provinciaSeleccionada;
	}
	
}