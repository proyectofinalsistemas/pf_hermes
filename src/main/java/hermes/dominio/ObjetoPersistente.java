package hermes.dominio;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public class ObjetoPersistente {
	@Id @GeneratedValue
	private Long id;
	protected boolean habilitado;

	public Long getId() {
		return id;
	}
	
	public void setHabilitado(boolean bool){
		this.habilitado= bool;
	}
}


