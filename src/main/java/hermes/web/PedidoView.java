package hermes.web;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import lombok.Getter;
import lombok.Setter;

import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

import Utilidades.Respuesta;
import Utilidades.Util;
import hermes.dominio.DetallePedido;
import hermes.dominio.EstadoPedido;
import hermes.dominio.MotivosCancelacion;
import hermes.dominio.Pedido;
import hermes.dominio.Producto;
import hermes.dominio.Sucursal;
import hermes.dominio.TipoEntrega;
import hermes.dominio.TipoPago;
import hermes.dominio.TipoProducto;
import hermes.logica.OrderService;
import hermes.logica.ProductService;

@RestController
@Scope("view")
public class PedidoView {
	@Autowired
	private OrderService _service;
	@Autowired
	private ProductService servicioProducto;
	private List<Pedido> pedidosHistoricos;
	private Pedido pedidoHistoricoSeleccionado;
	private List<Pedido> filtroPedidosHistoricos;
	private List<Pedido> pedidosActuales;
	private List<TipoPago> tipoPagos;
	private List<TipoEntrega> tipoEntregas;
	private @Getter @Setter List<Pedido> filtroPedidosActuales;
	private Pedido pedidoActualSeleccionado;
	private @Getter @Setter DetallePedido detallePedidoSeleccionado;
	private @Getter @Setter String tipoProductoSeleccionado;
	private @Getter @Setter String productoSeleccionado;
	private Long numeroPedido;
	private @Getter Date fechaDesde;
	private @Setter @Getter String fechaDesdeFormateada;
	private @Setter @Getter String fechaHastaFormateada; 
	private @Setter @Getter Date fechaHasta;
	private Sucursal sucursal;
	private BigDecimal total;
	private EstadoPedido estado;
	private boolean enviado;
	private List<DetallePedido> detalles;
	private List<Sucursal> sucursales;
	private @Setter @Getter List<Producto> productos;
	private @Setter @Getter List<TipoProducto> tipoProductos;
	private @Setter @Getter List<EstadoPedido> estados;
	private @Setter @Getter boolean noHaySeleccionado;
	private @Setter @Getter List<DetallePedido> filtroDetalles;
	private @Setter @Getter boolean noCancelable;
	private @Setter @Getter boolean noCobrable;
	private @Setter @Getter Integer cantidad;
	private @Setter Date maxDate;
	private @Setter @Getter boolean estaFiltrado;
	private @Setter @Getter boolean hayFechaMinima;
	private boolean isAdmin;
	private @Setter @Getter boolean noHaySeleccionadoDetalle;
	private @Setter @Getter List<MotivosCancelacion> motivosCancelacion;
	private @Setter @Getter String motivosCancelacionSeleccionado;
	private @Setter @Getter String descripcionCancelacion;
	private @Setter @Getter boolean pedidoNoModificable;
	private @Setter int hsMin;
	private @Setter int hsMax;
	private @Setter @Getter String lblSucursal;
	/*Modificaci�n de datos pedido*/
	private Date hsPedido;
	private Date hsEntrega;
	private String estadoNombre;
	private String sucursalSeleccionada;
	private String tipoPago;
	private String tipoEntrega;
	private String comentarios;
	private String calle;
	private String piso;
	private String dpto;
	private int numero;
	private boolean inicioPreparacion;
	private boolean finPreparacion;
	private String comentariosDetalle;
	
	@PostConstruct
	public void init() {
		this.cargarPedido();
		this.cargarSoporte();
		setNoHaySeleccionado(true);
		setNoHaySeleccionadoDetalle(true);
		setNoCancelable(true);
		setEstaFiltrado(false);
		setHayFechaMinima(false);
		cargarMotivosCancelacion();
		setNoCobrable(true);
		noEsAdmin();
		setPedidoNoModificable(true);
		setInicioPreparacion(false);
		setFinPreparacion(false);
	}

	public void modificarPedido() {
		pedidoActualSeleccionado.setHoraEntrega(hsEntrega);
		if(!(pedidoActualSeleccionado.getSucursal().toString()).equalsIgnoreCase(sucursalSeleccionada)) {
			pedidoActualSeleccionado.setSucursal(getSucursales().stream()
													.filter(s -> s.toString().equalsIgnoreCase(sucursalSeleccionada))
													.findFirst().orElse(null));
		}
		pedidoActualSeleccionado.setComentarios(getComentarios());
		if(!(pedidoActualSeleccionado.getTipoPago().toString()).equalsIgnoreCase(tipoPago)) {
			pedidoActualSeleccionado.setTipoPago(getTipoPagos().stream()
													.filter(tp -> tp.toString().equalsIgnoreCase(tipoPago))
													.findFirst().orElse(null));
		}
		if(!(pedidoActualSeleccionado.getTipoEntrega().toString()).equalsIgnoreCase(tipoEntrega)) {
			pedidoActualSeleccionado.setTipoEntrega(getTipoEntregas().stream()
													.filter(te -> te.toString().equalsIgnoreCase(tipoEntrega))
													.findFirst().orElse(null));
		}
		if(pedidoActualSeleccionado.getTipoEntrega().toString().equalsIgnoreCase("Env�o a Domicilio")) {
			pedidoActualSeleccionado.setCalle(getCalle());
			pedidoActualSeleccionado.setNumero(getNumero());
			pedidoActualSeleccionado.setPiso(getPiso());
			pedidoActualSeleccionado.setDpto(getDpto());
			//pedidoActualSeleccionado.setCoordenadas(_service.obtenerPuntoGeografico(getCalle(),getNumero()));
		}
		Respuesta rta = _service.modificarPedido(pedidoActualSeleccionado.getId(), pedidoActualSeleccionado);
		confirmacionCliente(rta);
	}
	
	
	public void confirmacionCliente(Respuesta rta) {
		if(rta.getEstado().equals("201")) {
			addMessage("Modificaci�n Exitosa", "Se ha modificado el pedido correctamente.");
		} else {
			addMessageError("Error al Modificar", ""+ rta.getMensajes());
		}
		Util.cerrarDialog("dlgModificarDatosPedido");
	}

	public void noEsAdmin() {
		setIsAdmin(_service.esAdmin());
	}

	public void filtroFecha() {
		if (!estaFiltrado) {
			Util.abrirDialog("dlgFiltroFecha");
		}	
	}
	
	public void reestablecerFecha() {
		setEstaFiltrado(false);
	}
	
	public void cargarPedido() {
		_service.setSucursalDeUsuario();
		pedidosActuales = _service.cargarPedidosActuales();
	}

	public void cargarPedidosHistoricos() {
		if (!fechaDesde.after(fechaHasta)) {
			pedidosHistoricos = _service.listarTodosPedidosHistoricos(
					getFechaDesde(), getFechaHasta());
			Util.abrirDialog("dlgFiltroFecha");
			estaFiltrado = true;
			formatearFechaDesde();
			formatearFechaHasta();
			Util.cerrarDialog("dlgFiltroFecha");
		} else {
			addMessageError("Error en filtrar fechas", "Fecha Inicial mayor a Final.");
		}
	}
	
	public void formatearFechaDesde(){
		SimpleDateFormat formateada = new SimpleDateFormat("dd/MM/yyyy");
		setFechaDesdeFormateada(formateada.format(getFechaDesde()));
	}
	
	public void formatearFechaHasta(){
		SimpleDateFormat formateada = new SimpleDateFormat("dd/MM/yyyy");
		setFechaHastaFormateada(formateada.format(getFechaHasta()));
	}
	
	public boolean verificarHasta() {
		if (fechaDesde.before(fechaHasta)) {
			return true;
		} else {
			return false;
		}
	}

	public void cargarSoporte() {
		setSucursales(_service.getSucursalesHabilitadas());
		setEstados(_service.listarEstados());
	}
	
	public void cargarMotivosCancelacion(){
		setMotivosCancelacion(_service.listarMotivosCancelacion());
	}

	public void obtenerDetalles() {
		setDetalles(pedidoHistoricoSeleccionado.getDetalle());
		setInicioPreparacion(inicioPreparacion());
		setFinPreparacion(finPreparacion());
		cargarProductos();
		Util.abrirDialog("dlgMostrarDetallePedido");
	}

	public void obtenerDetallesActuales() {
		setDetalles(pedidoActualSeleccionado.getDetalle());
		setInicioPreparacion(inicioPreparacion());
		setFinPreparacion(finPreparacion());
		Util.abrirDialog("dlgMostrarDetallePedido");
	}

	public void cargarProductos() {
		setProductos(servicioProducto.listarTodosProducto());
		setTipoProductos(servicioProducto.listarTodosTipoProducto());
	}

	public void rowDblSelect() {
		obtenerDetalles();
	}

	public void filtrarProductos() {
		productos = servicioProducto
				.listarProductosPorTipoProducto(tipoProductoSeleccionado);
	}
	

	public void addMessage(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
				summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void addMessageError(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void onRowSelect(SelectEvent event) {
		this.noHaySeleccionado = false;
	}
	public void onRowSelectActual(SelectEvent event) {
		this.noHaySeleccionado = false;
		verificarEstado();
	}
	public void onRowSelectDetalleActual(SelectEvent event){
		if((detallePedidoSeleccionado.getEstado().getNombre().equals("En Espera")) ||
				(detallePedidoSeleccionado.getEstado().getNombre().equals("En Preparaci�n"))){
			setNoHaySeleccionadoDetalle(false);
			setPedidoNoModificable(false);
		} else {
			setNoHaySeleccionadoDetalle(true);
			setPedidoNoModificable(true);
		}
	}
	
	public void rowDblSelectActuales() {
		prepararModificacionDatos();
		obtenerDetallesActuales();
	}
	
	public void rowDblSelectActual() {
		Util.abrirDialog("dlgMostrarPedido");
	}
	
	private void obtenerPedidoActual() {
		setPedidoActualSeleccionado(pedidoActualSeleccionado);
	}

	public void prepararCancelacion(){
		Util.abrirDialog("dlgCancelarPedido");
	}
	public void cancelarPedido() {
		_service.cancelar(pedidoActualSeleccionado,motivosCancelacionSeleccionado, descripcionCancelacion);
		addMessage("Cancelado con exito", "Pedido cancelado");
		Util.cerrarDialog("dlgCancelarPedido");
	}
	
	public void cancelarDetalle(){
		_service.cancelar(pedidoActualSeleccionado, detallePedidoSeleccionado, motivosCancelacionSeleccionado, descripcionCancelacion);
		addMessage("Cancelado con exito", "Elemento del Pedido cancelado");
		Util.cerrarDialog("dlgCancelarDetalle");
		vaciarCampos();
	}
	public void prepararCobro(){
		Util.abrirDialog("dlgRegistrarCobro");
	}
	
	public void registrarCobro(){
		_service.registrarCobro(pedidoActualSeleccionado);
		addMessage("Cobro Registrado", "El cobro ha sido registrado con �xito.");
		Util.cerrarDialog("dlgRegistrarCobro");
	}
	public Date getMaxDate() {
		Calendar fecha = Calendar.getInstance();
		return maxDate = fecha.getTime();
	}

	public void verificarEstado() {
		verificarCobrable();
		if ((pedidoActualSeleccionado.getEstado().getNombre()).equalsIgnoreCase("En Espera")) {
			setNoCancelable(false);
		} else {
			setNoCancelable(true);
		}
		//vaciarCampos();
	}

	public void verificarCobrable() {
		if ((pedidoActualSeleccionado.getEstado().getNombre()).equals("Preparado")){
			setNoCobrable(false);
		} else {
			setNoCobrable(true);
		}
	}

	public List<Pedido> getPedidosHistoricos() {
		return pedidosHistoricos;
	}

	public void vaciarCampos() {
		tipoProductoSeleccionado = "";
		productoSeleccionado = "";
		cantidad = 0;
		cargarProductos();
		filtrarProductos();
		setMotivosCancelacionSeleccionado("");
		setDescripcionCancelacion("");
		setNoHaySeleccionadoDetalle(true);
		setDetallePedidoSeleccionado(null);
		setPedidoNoModificable(true);
		setNumeroPedido(null);
		setHsPedido(null);
		setHsEntrega(null);
		setEstadoNombre("");
		setSucursalSeleccionada("");
		setComentarios("");
		setTipoPago("");
		setTotal(BigDecimal.ZERO);
		setTipoEntrega("");
		setCalle("");
		setNumero(0);
		setPiso("");
		setDpto("");
		setInicioPreparacion(false);
		setFinPreparacion(false);
	}
	
	public void prepararModificacionDatos() {
		setNumeroPedido(pedidoActualSeleccionado.getId());
		setHsPedido(pedidoActualSeleccionado.getHoraPedido());
		setHsEntrega(pedidoActualSeleccionado.getHoraEntrega());
		setEstadoNombre(pedidoActualSeleccionado.getEstado().getNombre());
		setSucursalSeleccionada(pedidoActualSeleccionado.getSucursal().getNombre());
		setComentarios(pedidoActualSeleccionado.getComentarios());
		setTipoPago(pedidoActualSeleccionado.getTipoPago().getNombre());
		setTotal(pedidoActualSeleccionado.getTotal());
		setTipoEntrega(pedidoActualSeleccionado.getTipoEntrega().getNombre());
		if(getTipoEntrega().equalsIgnoreCase("Env�o a Domicilio")) {
			setCalle(pedidoActualSeleccionado.getCalle());
			setNumero(pedidoActualSeleccionado.getNumero());
			setPiso(pedidoActualSeleccionado.getPiso());
			setDpto(pedidoActualSeleccionado.getDpto());
		}
		Util.abrirDialog("dlgMostrarPedido");
	}
	
	public void cambiarEnvio() {
		pedidoActualSeleccionado.setTipoEntrega(getTipoEntregas().stream().filter(te -> te.getNombre().equalsIgnoreCase(getTipoEntrega())).findFirst().orElse(null));
	}
	
	public void validarEstadoModificacion() {
		setTipoPagos(_service.obtenerTipoPagoHabilitados());
		setTipoEntregas(_service.obtenerTipoEntregaHabilitadas());
		if((getPedidoActualSeleccionado().getEstado().getNombre()).equalsIgnoreCase("En Preparaci�n")){
			Util.abrirDialog("dlgValidarModificacionPedido");
		} else {
			Util.abrirDialog("dlgModificarDatosPedido");
		}
	}
	
	public void validoModificacionPedido(){
		//Registrar en el historial de modificaciones.
		Util.abrirDialog("dlgModificarDatosPedido");
		Util.cerrarDialog("dlgValidarModificacionPedido");
	}
	
	public void prepararModificacion(){
		cargarProductos();
		setTipoProductoSeleccionado(detallePedidoSeleccionado.getNombreTipoProducto());
		setProductoSeleccionado(detallePedidoSeleccionado.getNombreProducto());
		setCantidad(detallePedidoSeleccionado.getCantidad());
		setComentariosDetalle(detallePedidoSeleccionado.getComentarios());
		filtrarProductos();
		if((getDetallePedidoSeleccionado().getEstado().getNombre()).equals("En Preparaci�n")){
			Util.abrirDialog("dlgValidarModificacion");
		} else {
			Util.abrirDialog("dlgModificarDetalle");
		}
	}
	public void validoModificacion(){
		//Registrar en el historial de modificaciones.
		Util.abrirDialog("dlgModificarDetalle");
		Util.cerrarDialog("dlgValidarModificacion");
	}
	public void modificarDetalle(){
		Producto prod = getProductos().stream()
						.filter(p -> p.getNombre().equalsIgnoreCase(productoSeleccionado) && p.getTipoProducto().getNombre().equalsIgnoreCase(tipoProductoSeleccionado))
						.collect(Collectors.toList()).get(0);
		if(prod!=null) {
			if(!((getDetallePedidoSeleccionado().getProducto().equals(prod))&&(getDetallePedidoSeleccionado().getCantidad().equals(getCantidad()))
					&& getComentariosDetalle().isEmpty())){
				detallePedidoSeleccionado.setProducto(prod);
				detallePedidoSeleccionado.setCantidad(getCantidad());
				detallePedidoSeleccionado.setComentarios(getComentariosDetalle());
				detallePedidoSeleccionado.setNombreProducto(prod.getNombre());
				detallePedidoSeleccionado.setNombreTipoProducto(prod.getTipoProducto().getNombre());
				detallePedidoSeleccionado.setPrecioProducto(prod.getPrecio());
				_service.modificarDetalle(getPedidoActualSeleccionado(), getDetallePedidoSeleccionado());
			}else{
				Util.addMessage("Detalle No Modificado", "No se modific� ning�n dato. ");
			}
		}else Util.addMessage("Detalle No Modificado", "Hubo un inconveniente con la modificaci�n del Detalle. Comun�quese con su proveedor de sistemas.");
		Util.cerrarDialog("dlgModificarDetalle");
		vaciarCampos();
	}
		
	public void setPedidosHistoricos(List<Pedido> pedidosHistoricos) {
		this.pedidosHistoricos = pedidosHistoricos;
	}

	public Pedido getPedidoHistoricoSeleccionado() {
		return pedidoHistoricoSeleccionado;
	}

	public void setPedidoHistoricoSeleccionado(Pedido pedidosHistoricosSeleccionado) {
		this.pedidoHistoricoSeleccionado = pedidosHistoricosSeleccionado;
	}

	public List<Pedido> getFiltroPedidosHistoricos() {
		return filtroPedidosHistoricos;
	}

	public void setFiltroPedidosHistoricos(List<Pedido> filtroPedidosHistoricos) {
		this.filtroPedidosHistoricos = filtroPedidosHistoricos;
	}

	public Long getNumeroPedido() {
		return numeroPedido;
	}

	public void setNumeroPedido(Long numeroPedido) {
		this.numeroPedido = numeroPedido;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public EstadoPedido getEstado() {
		return estado;
	}

	public void setEstado(EstadoPedido estado) {
		this.estado = estado;
	}

	public boolean getEnviado() {
		return enviado;
	}

	public void setEnviado(boolean enviado) {
		this.enviado = enviado;
	}

	public List<DetallePedido> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<DetallePedido> detalles) {
		this.detalles = detalles;
	}

	public List<Pedido> getPedidosActuales() {
		return pedidosActuales;
	}

	public void setPedidosActuales(List<Pedido> pedidosActuales) {
		this.pedidosActuales = pedidosActuales;
	}

	public Pedido getPedidoActualSeleccionado() {
		return pedidoActualSeleccionado;
	}

	public void setPedidoActualSeleccionado(Pedido pedidoActualSeleccionado) {
		this.pedidoActualSeleccionado = pedidoActualSeleccionado;
	}

	public List<Sucursal> getSucursales() {
		return sucursales;
	}

	public void setSucursales(List<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public boolean getIsEnvio() {
		 if(!(pedidoActualSeleccionado==null)) return pedidoActualSeleccionado.getTipoEntrega().getNombre().equalsIgnoreCase("Env�o a Domicilio");
		 return false;
	}

	public int getHsMax() {
		Calendar tmp = Calendar.getInstance();
		if(tmp.get(Calendar.HOUR_OF_DAY) > (14)) {
			return hsMax = 23;
		} else {
	        return hsMax = 13;	
		}
	}

	public int getHsMin(){
		Calendar tmp = Calendar.getInstance();
		if(tmp.get(Calendar.HOUR_OF_DAY) > (14)) {
	        return hsMin = 19;
		} else {
	        return hsMin = 11;	
		}
	}
	
	public boolean inicioPreparacion() {
	return ((pedidoActualSeleccionado.getEstado().getNombre().equalsIgnoreCase("En Preparaci�n"))||(pedidoActualSeleccionado.getEstado().getNombre().equalsIgnoreCase("Preparado"))||
				(pedidoActualSeleccionado.getEstado().getNombre().equalsIgnoreCase("Cobrado"))||(pedidoActualSeleccionado.getEstado().getNombre().equalsIgnoreCase("Anulado")));
	}

	public boolean finPreparacion() {
	return ((pedidoActualSeleccionado.getEstado().getNombre().equalsIgnoreCase("Preparado"))||(pedidoActualSeleccionado.getEstado().getNombre().equalsIgnoreCase("Cobrado"))
				||(pedidoActualSeleccionado.getEstado().getNombre().equalsIgnoreCase("Anulado")));
	}
	
	public Date getHsPedido() {
		return hsPedido;
	}

	public void setHsPedido(Date hsPedido) {
		this.hsPedido = hsPedido;
	}

	public Date getHsEntrega() {
		return hsEntrega;
	}

	public void setHsEntrega(Date hsEntrega) {
		this.hsEntrega = hsEntrega;
	}

	public String getEstadoNombre() {
		return estadoNombre;
	}

	public void setEstadoNombre(String estadoNombre) {
		this.estadoNombre = estadoNombre;
	}

	public String getSucursalSeleccionada() {
		return sucursalSeleccionada;
	}

	public void setSucursalSeleccionada(String sucursalSeleccionada) {
		this.sucursalSeleccionada = sucursalSeleccionada;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public String getTipoEntrega() {
		return tipoEntrega;
	}

	public void setTipoEntrega(String tipoEntrega) {
		this.tipoEntrega = tipoEntrega;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public List<TipoPago> getTipoPagos() {
		return tipoPagos;
	}

	public void setTipoPagos(List<TipoPago> tipoPagos) {
		this.tipoPagos = tipoPagos;
	}

	public List<TipoEntrega> getTipoEntregas() {
		return tipoEntregas;
	}

	public void setTipoEntregas(List<TipoEntrega> tipoEntregas) {
		this.tipoEntregas = tipoEntregas;
	}

	public boolean isInicioPreparacion() {
		return inicioPreparacion;
	}

	public void setInicioPreparacion(boolean inicioPreparacion) {
		this.inicioPreparacion = inicioPreparacion;
	}

	public boolean isFinPreparacion() {
		return finPreparacion;
	}

	public void setFinPreparacion(boolean finPreparacion) {
		this.finPreparacion = finPreparacion;
	}

	public String getComentariosDetalle() {
		return comentariosDetalle;
	}

	public void setComentariosDetalle(String comentariosDetalle) {
		this.comentariosDetalle = comentariosDetalle;
	}
}
