package hermes.persistencia;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.Producto;
import hermes.dominio.TipoProducto;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Producto, Integer> {

	public List<Producto> findByNombreAndTipoProducto(String nombre, TipoProducto tipoProducto);
	public List<Producto> findByTipoProducto(TipoProducto tipoProducto);
	public List<Producto> findByHabilitado(boolean habilitado);
	public List<Producto> findByHabilitadoOrderByTipoProductoIdAsc(boolean habilitado);
	public List<Producto> findByTipoProductoNombre(String nombre);
	public Producto findById(Integer id);
	public Producto findByIdAndHabilitado(Integer id, boolean b);
	public List<Producto> findByTipoProductoIdAndHabilitado(Integer id, boolean b);
	public List<Producto> findByHabilitadoAndTipoProductoHabilitado(boolean b, boolean c);
}
