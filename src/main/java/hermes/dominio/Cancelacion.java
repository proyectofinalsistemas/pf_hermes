package hermes.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity
public class Cancelacion {
	@Id @GeneratedValue
	private @NonNull Integer id;
	private String descripcion;
	@OneToOne @NotNull 
	private @NonNull MotivosCancelacion motivo;
	@ManyToOne @NotNull 
	private @NonNull Pedido pedido;
	@OneToOne @NotNull 
	private @NonNull DetallePedido detalle;
	
	public Cancelacion(String descripcion, Pedido pedido, DetallePedido detalle, MotivosCancelacion motivo){
		setDescripcion(descripcion);
		setPedido(pedido);
		setDetalle(detalle);
		setMotivo(motivo);
	}
}
