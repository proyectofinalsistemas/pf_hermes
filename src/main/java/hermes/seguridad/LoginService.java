package hermes.seguridad;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import hermes.persistencia.UserRepository;

@Component
public class LoginService implements UserDetailsService {
	
	@Autowired
	private UserRepository repositorioUsuario;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return (UserDetails) repositorioUsuario.findByUsuario(username).get(0);
	}

}
