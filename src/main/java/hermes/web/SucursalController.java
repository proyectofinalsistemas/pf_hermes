package hermes.web;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hermes.dominio.Sucursal;
import hermes.logica.BranchService;

@RestController
@RequestMapping("/sucursal")
@CrossOrigin(origins = "*")
public class SucursalController {

	@Autowired
	private BranchService _servicio;
	
	// CALL CONSULTAS
	@GetMapping
	public List<Sucursal> getAllSucursales(){
		return _servicio.listarTodasSucursales();
	}

	@RequestMapping(value = "/habilitadas", method = RequestMethod.GET)
	public List<Sucursal> getSucursalesHabilitadas(){
		return _servicio.listarTodasHabilitadas();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Sucursal getSucursal(@PathVariable(name = "id") Integer id){
		return _servicio.obtenerSucursalSinGeolocalizacion(id);
	}
}
