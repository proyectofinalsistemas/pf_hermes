package Utilidades;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletContext;

import org.primefaces.context.RequestContext;

public class Util {
    
	private static final String username = "hermesdeveloped@gmail.com";		//Cuenta desde la cuál se envían los mails.
    private static final String password = "pavkqdjepqjocglc";				//Password necesaria para enviar mails desde la aplicación a través de la cuenta de arriba.

    
	public static String estandarizarPalabras(String palabras) { // Este método tiene la funcionalidad de estandarizar palabras antes de ser guardadas.
		String texto = (palabras.trim()).toLowerCase();			 // Obtiene la palabra/texto a ser estandarizado, quita espacios y lo convierte a minúsculas.
		texto = (texto.substring(0, 1)).toUpperCase()+(texto.substring(1)); // A la primera letra la setea a mayúsculas.
		boolean estandarizado=false;							 //Declaración y inicialización de variables.
		int posEspacio=-1;
		while (!estandarizado) {								 //Ciclo para buscar todos los espacios dentro del texto y pasar a mayúsculas su próximo caracter.
			posEspacio = texto.indexOf(" ", posEspacio+1);
			if ((posEspacio<texto.length()) && ( posEspacio > -1)){
				texto = (texto.substring(0, posEspacio+1)) + ((texto.substring(posEspacio+1, posEspacio+2)).toUpperCase()) + (texto.substring(posEspacio+2));
			} else{
				estandarizado=true;
			}
		}
		return texto;
	}
    
	public static void addMessage(String summary, String detail) {								//Se podría unificar los addMessage solicitando además del summary y detail un nro que defina si es Info, Error, o algún otro.
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	public static void addMessageError(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
		
	public static String removeAcentos(String s) {
	        if (s == null) {
	            return "";
	        }
	        String semAcentos = s.toLowerCase();
	        semAcentos = semAcentos.replaceAll("[áàâãä]", "a");
	        semAcentos = semAcentos.replaceAll("[éèêë]", "e");
	        semAcentos = semAcentos.replaceAll("[íìîï]", "i");
	        semAcentos = semAcentos.replaceAll("[óòôõö]", "o");
	        semAcentos = semAcentos.replaceAll("[úùûü]", "u");
	        semAcentos = semAcentos.replaceAll("ç", "c");
	        semAcentos = semAcentos.replaceAll("ñ", "n");
	        return semAcentos;
	}
	
	public static void enviarMail(String mail, String asunt, String msj) {		//Método para enviar un mail.
	    String mailDestinatario=mail;
	    String asunto=asunt;
	    String mensaje=msj;
        try {
            Properties props = new Properties();								//Se setean las propiedades internas necesarias.
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.user", "usuario");
            props.put("mail.smtp.port", 587);
            //Se crea una sesión con las credenciales de la cuenta desde la que enviaremos el mail.
            SMTPAuthenticator auth = new SMTPAuthenticator(username, password);	//Estos parámetros, especificados en los atributos de la clase, son las credenciales de la cuenta desde la que enviaremos dicho mail.
            Session session = Session.getInstance(props, auth);
            session.setDebug(false);
            //Creamos un nuevo mensaje, y le pasamos nuestra sesión iniciada en el paso anterior.
            MimeMessage mimemessage = new MimeMessage(session);
            InternetAddress addressFrom = new InternetAddress(username);
            mimemessage.setFrom(addressFrom);
            mimemessage.setRecipients(Message.RecipientType.TO, mailDestinatario);
            mimemessage.setSubject(asunto);
            // Se crea el contenido del mensaje
            //Y por ultimo el texto.
            MimeBodyPart mimebodypart = new MimeBodyPart();
            mimebodypart.setText(mensaje);
            mimebodypart.setContent(mensaje, "text/html; charset=utf-8");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimebodypart);
            mimemessage.setContent(multipart);
            mimemessage.setSentDate(new Date());
            Transport.send(mimemessage);

            //Con esta imprimimos en consola que el mensaje fue enviado
            System.out.println("Mensaje Enviado");
        } catch (MessagingException e) {
            //Si existiera un error en el envío lo hacemos saber al cliente y lanzamos una excepcion.

            System.out.println("Hubo un error al enviar el mensaje.");
            throw new RuntimeException(e);
        }
        
    }
		
	public static String sha512(String cadena) {  					 //Método de hasheo. Recibe una cadena y devuelve el hash aplicado resultante a la misma.
        StringBuilder sb = new StringBuilder();

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512"); //Define el método de hash.

            md.update(cadena.getBytes());

            byte[] mb = md.digest();

            for (int i = 0; i < mb.length; i++) {
                sb.append(Integer.toString((mb[i] & 0xff) + 0x100, 16).substring(1));
            }
        } catch (NoSuchAlgorithmException ex) {
            /*...*/
        }

        return sb.toString();
    }
		
    public static String guardarFichero(byte[] bytes, String nombreArchivo){				//Método para generar la ubicación de la imágen a guardar. Genera la url que se termina guardando en BD a modo que el sistema al cargar el objeto busque el archivo en dicha ubicación.
        String ubicacionImagen=null;
        ServletContext servletContext =(ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String Path=servletContext.getRealPath("") +File.separatorChar + "resources" + File.separatorChar + "imagenes" + File.separatorChar + "Productos"+File.separatorChar +nombreArchivo;
        File f = null;
        InputStream in=null;
        try{
            f=new File(Path);
            in=new ByteArrayInputStream(bytes);
            FileOutputStream out = new FileOutputStream(f.getAbsoluteFile());
            int c=0;
            while((c=in.read())>=0){
                out.write(c);
            }
            out.flush();
            out.close();
            ubicacionImagen="../../resources/imagenes/Productos/"+nombreArchivo;
        }catch(Exception e){
            System.err.println("No se pudo cargar");
        }
        return ubicacionImagen;
    }

	public static String generarPassword() {							//Método para generar passwords aleatorias. Definido para passwords de 8 elementos, alfanuméricos -Minúsculas, Mayúsculas y Números-. 
		String NUMEROS = "0123456789";
		String MAYUSCULAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";
		
		String pswd = "";
		for (int i = 0; i < 8; i++) {
			String key = NUMEROS + MAYUSCULAS + MINUSCULAS;
			pswd+=((key).charAt((int)(Math.random() * key.length())));
		}
	 	return pswd;
	}

	public static void abrirDialog(String dialog) {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('" + dialog + "').show();");
	}
	
	public static void cerrarDialog(String dialog) {
		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("PF('" + dialog + "').hide()");
	}
}
