package hermes.persistencia;

import java.util.List;

import hermes.dominio.Ciudad;
import hermes.dominio.Sucursal;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BranchRepository extends PagingAndSortingRepository<Sucursal, Integer> {
	
	public Sucursal findByNombre(String nombre);
	public List<Sucursal> findByNombreOrCalleAndNumeroAndCiudad(String nombre, String calle, int numero, Ciudad ciudad);
	public List<Sucursal> findByCalleAndNumeroAndCiudad(String calle, int numero, Ciudad ciudad);
	public List<Sucursal> findByHabilitado(boolean b);
	
//	@Query (
//			value ="SELECT s.id,s.nombre,s.descripcion,s.telefono,s.habilitado,s.calle,s.numero,st_astext(p.punto) FROM Sucursal s JOIN s.geolocalizacion p", 
//			nativeQuery=true)
//	public List<Sucursal> getAllSucursales();
}