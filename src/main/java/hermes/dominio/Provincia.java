package hermes.dominio;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity 
public class Provincia {
	@Id
	private @NonNull Integer id;
	private @NonNull String nombre;
	private String descripcion;
	
	@Override
	public String toString(){
		return nombre;
	}
}
