package hermes.persistencia;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.TipoEntrega;

@Repository
public interface DelliveryTypeRepository extends PagingAndSortingRepository<TipoEntrega,Integer> {

	List<TipoEntrega> findByHabilitado(boolean b);

}
