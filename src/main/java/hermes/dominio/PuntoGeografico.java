package hermes.dominio;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.primefaces.model.map.LatLng;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

import Utilidades.Util;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) 
@Entity
public class PuntoGeografico {

	@Id @GeneratedValue
	private Integer id;
	
	@Column(columnDefinition="Geometry", unique=true, nullable=false) 
	@Type(type = "org.hibernate.spatial.GeometryType") 
	private Point punto; 
	private double latitud; 
	private double longitud; 
	private static final int srid=3857;
	
	public PuntoGeografico(LatLng pto){
		Coordinate coord = new Coordinate(pto.getLat(),pto.getLng());
		PrecisionModel pm = new PrecisionModel();
		GeometryFactory gf = new GeometryFactory(pm,getSrid());
		setLatitud(pto.getLat());
		setLongitud(pto.getLng());
		setPunto(gf.createPoint(coord));
	}
	public PuntoGeografico(double lat, double lng) {
		Coordinate coord = new Coordinate(lat,lng);
		PrecisionModel pm = new PrecisionModel();
		GeometryFactory gf = new GeometryFactory(pm,getSrid());
		setLatitud(lat);
		setLongitud(lng);
		setPunto(gf.createPoint(coord));
	}
	@Override
	public String toString(){
		return "Point("+ punto.getX() + ";"+ punto.getY() +")";
	}
	
	//Dado un String que representa el tipo, devuelve un tipo Geometry que representa.
	public Geometry wktToGeometry(String wkt){
		WKTReader text = new WKTReader();
		Geometry geom = null;
		try {
			geom = text.read(wkt);
		} catch (ParseException e){
			throw new RuntimeException("No es un texto WKT reconocido: "+wkt);
		}
		return geom;
	}
	
	public int getSrid(){
		return this.srid;
	}
}
