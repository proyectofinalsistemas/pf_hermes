package hermes.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity
public class Rol implements GrantedAuthority{
	@Id @GeneratedValue
	private Integer id;
	protected boolean habilitado;

	@NotNull
	private @NonNull String nombre;
	private @NonNull String descripcion;
	
	@Override
	public String toString(){
		return nombre;
	}

	@Override
	public String getAuthority() {
		return nombre;
	}

	public Integer getId() {
		return id;
	}
	
	public void setHabilitado(boolean bool){
		this.habilitado= bool;
	}
//	@OneToOne
//	private Permiso permiso;
}
