package hermes.dominio;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor @RequiredArgsConstructor
@Entity @EqualsAndHashCode(callSuper=true)
public class ImagenProducto extends ObjetoPersistente{
	private @NonNull byte[] contenido;
	private @NonNull String tipo;
	@OneToOne
	private @NonNull Producto producto;
}
