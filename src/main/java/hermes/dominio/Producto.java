package hermes.dominio;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity
public class Producto {
	@Id @GeneratedValue
	private Integer id;
	@NotNull
	private @NonNull String nombre;
	private String descripcion;
	@Min(0) @NotNull
	private @NonNull BigDecimal precio;
	private boolean habilitado;
	private String imagen;
	@ManyToOne @NotNull
	private @NonNull TipoProducto tipoProducto;
	
	public Producto(String nombre, String descripcion, BigDecimal precio, String imagen, TipoProducto tipoProducto, boolean estado){
		this.setNombre(nombre);
		this.setDescripcion(descripcion);
		this.setPrecio(precio);
		this.setImagen(imagen);
		this.setTipoProducto(tipoProducto);
		this.setHabilitado(estado);
	}
	
	public boolean getHabilitado(){
		return this.habilitado;
	}

	public Integer getId() {
		return id;
	}
	
	public void setHabilitado(boolean bool){
		this.habilitado= bool;
	}	
	public String toString(){
		return this.nombre;
	}
}
