package hermes.web;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.map.GeocodeEvent;
import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.event.map.ReverseGeocodeEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.GeocodeResult;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.primefaces.model.map.Polygon;
import org.primefaces.model.map.Polyline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

import Utilidades.Util;
import hermes.dominio.Sucursal;
import hermes.logica.GeolocationService;
import lombok.Getter;
import lombok.Setter;

@RestController
@Scope("view")
public class GeocodeView {
     
	@Autowired
	private GeolocationService servicio;
    private MapModel geoModel;
    private String centerGeoMap = "-32.410344, -63.243392";
    private @Setter @Getter LatLng sucursal;
    private Marker marker; 
    private @Setter @Getter boolean agregarPto;
    private @Setter @Getter boolean confirmarPto;
    private int contadorMarks;
    private Polyline polilinea;
    private Polygon poligono;
    private @Setter @Getter boolean hayZona;
    private boolean noHayZona;
    private boolean zoneable;
    
    @PostConstruct
    public void init() {
        geoModel = new DefaultMapModel();
        setAgregarPto(true);
        setConfirmarPto(true);
        setHayZona(true);
        contadorMarks=0;
    	//limpiarMarcas();
    	polilinea = new Polyline();
    	poligono = new Polygon();
    	//setZoneable(true);
    }
     
    public void onGeocode(GeocodeEvent event) {												//Esta funcionalidad trae las coordenadas a partir de la direcci�n escrita.
    	List<GeocodeResult> results = event.getResults();
         
        if (results != null && !results.isEmpty()) {
            LatLng center = results.get(0).getLatLng();
            setSucursal(center);
            centerGeoMap = center.getLat() + "," + center.getLng();
             
            for (int i = 0; i < results.size(); i++) {
                GeocodeResult result = results.get(i);
                geoModel.addOverlay(new Marker(result.getLatLng(), result.getAddress()));
            }
            for(Marker premarker : geoModel.getMarkers()) {
                premarker.setDraggable(true);
            }
        }
        
    }
    
//    public void onReverseGeocode(ReverseGeocodeEvent event) {								//Esta funcionalidad permite ser ingresadas las coordenadas directamente y las ubica en el mapa.
//        List<String> addresses = event.getAddresses();
//        LatLng coord = event.getLatlng();
//         
//        if (addresses != null && !addresses.isEmpty()) {
//            centerRevGeoMap = coord.getLat() + "," + coord.getLng();
//            revGeoModel.addOverlay(new Marker(coord, addresses.get(0)));
//        }
//    }

    public void onPointSelect(PointSelectEvent event) {
        LatLng latlng = event.getLatLng();
        Util.addMessage("Punto a�adido", "La latitud y longitud es: "+latlng.toString());
        geoModel.addOverlay(new Marker(latlng));
        contadorMarks++;
        geoModel.getMarkers().get(contadorMarks).setDraggable(true);
        geoModel.getMarkers().get(contadorMarks).setIcon("http://maps.google.com/mapfiles/ms/micons/blue-dot.png");
        if (contadorMarks==2){
        	agregarPolilinea();
        }else if (contadorMarks>2) {
        		agregarPoligono();
        	}
    }
    
	public void limpiarMarcas() {
    	geoModel.getMarkers().clear();
    	setAgregarPto(true);
    	setConfirmarPto(false);
	}

	public void onMarkerDrag(MarkerDragEvent event) {
        marker = event.getMarker();
        int i=0;
        for(Marker m : geoModel.getMarkers()){
        	if(m.getId().equals(marker.getId())){
        		geoModel.getMarkers().get(i).setLatlng(marker.getLatlng());
        	}
        	i++;
        }
        geoModel.getPolygons().clear();
        geoModel.getPolylines().clear();
        agregarPoligono();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Marker Dragged", "Lat:" + marker.getLatlng().getLat() + ", Lng:" + marker.getLatlng().getLng()));
    }
    public MapModel getGeoModel() {
        return geoModel;
    }
    public String getCenterGeoMap() {
        return centerGeoMap;
    }

    public void agregarPunto(){
    	setAgregarPto(false);
    	setConfirmarPto(true);
    	Util.addMessage("Funciona", "Mensaje");
    }
    
    public void modificarPtos(){
    	setAgregarPto(true);
    	setConfirmarPto(false);
    }
    
    public void confirmarPunto(){
    	setAgregarPto(true);
    	setConfirmarPto(false);
    	setHayZona(true);
    	Util.addMessage("Confirmado", "Mensaje");
    	servicio.setMapa(getGeoModel());
    	servicio.setHayZona(isHayZona());
    }
    
	public void agregarUltimoPto() {
		LatLng coord = new LatLng((geoModel.getMarkers().get(1).getLatlng().getLat()),(geoModel.getMarkers().get(1).getLatlng().getLng()));		
		polilinea.getPaths().add(coord);
		poligono.getPaths().add(coord);
		geoModel.addOverlay(polilinea);
		geoModel.addOverlay(poligono);
	}

	private void agregarPolilinea() {
		int i=0;
		for (Marker m : geoModel.getMarkers()){
			if(i>0){
				LatLng coord = new LatLng((m.getLatlng().getLat()), (m.getLatlng().getLng()));
				polilinea.getPaths().add(coord);
			} else {								
				//La intenci�n de este If es simplemente descartar la primera marca, la cual es la ubicaci�n de la sucursal en s�.
				
				i=1;								
			}										
		}											
		//limpio las propiedades de las polil�neas, para que no se solapen.
//		polilinea.setStrokeWeight(0);
//		polilinea.setStrokeColor("#000000");
//		polilinea.setStrokeOpacity(0);
		//Seteo las propiedades para que queden visualmente correctas.
        polilinea.setStrokeWeight(10);
        polilinea.setStrokeColor("#FF9900");
        polilinea.setStrokeOpacity(0.7);
//        
//        geoModel.addOverlay(polilinea);
	}
	
	private void agregarPoligono() {
		int i=0;
		agregarPolilinea();
		for (Marker m : geoModel.getMarkers()){
			if (i>0){
				LatLng coord = new LatLng((m.getLatlng().getLat()),(m.getLatlng().getLng()));
				poligono.getPaths().add(coord);
			} else {i=1;}							//La intenci�n de este If es simplemente descartar la primera marca, la cual es la ubicaci�n
		}											//de la sucursal en s�.
		//limpio las propiedades de los pol�gonos, para que no se solapen.
//		poligono.setStrokeColor("000000");
//		poligono.setStrokeOpacity(0);
//		poligono.setFillColor("000000");
//		poligono.setFillOpacity(0);
		//Seteo las propiedades para que queden visualmente correctas.
		poligono.setStrokeColor("#FF9900");
        poligono.setFillColor("#FF9900");
        poligono.setStrokeOpacity(0.3);
        poligono.setFillOpacity(0.3);

    	agregarUltimoPto();
//        geoModel.addOverlay(poligono);

	}
	
	public void poligonoSeleccionado(){
		Util.addMessage("Acci�n no realizada", "El punto seleccionado ya forma parte de la zona de influencia de la sucursal.");
	}

	public void limpiarMapa() {
		limpiarMarcas();
		limpiarPoligonos();
		limpiarPolilineas();
		setHayZona(false);
	}

	public void limpiarPolilineas() {
		geoModel.getPolylines().clear();
	}

	public void limpiarPoligonos() {
		geoModel.getPolygons().clear();
	}

	public boolean hayZona() {
		return hayZona;
	}
	public boolean getNoHayZona(){
		return !hayZona();
	}

	public boolean getZoneable() {
		return zoneable;
	}

	public void setZoneable(boolean b) {
		
		this.zoneable = b;
	}
}