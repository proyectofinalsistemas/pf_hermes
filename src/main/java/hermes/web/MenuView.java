package hermes.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

import hermes.dominio.Producto;
import hermes.dominio.TipoProducto;
import hermes.logica.ProductService;

@RestController
@Scope("view")
public class MenuView {
	@Autowired
	private ProductService servicio;
    private MenuModel model;
	private Iterable<TipoProducto> tipoProductos;
    @PostConstruct
    public void init() {
        model = new DefaultMenuModel();
        DefaultSubMenu tituloMenu = new DefaultSubMenu("Menu");
        
        DefaultMenuItem itema = new DefaultMenuItem("Todos");
    	itema.setCommand("#{dataScrollerView.getTodosProductos()}");
//      item.setIcon("");
    	itema.setUpdate(":formProducto");
        tituloMenu.addElement(itema);
        
        for(TipoProducto tipo: servicio.listarTodosTipoProductoHabilitados()){
        	DefaultMenuItem item = new DefaultMenuItem(tipo.getNombre());
        	item.setCommand("#{dataScrollerView.getProductosFiltrados("+"'"+tipo.getNombre()+"'"+")}"); // el metodo getProductosFiltrados Necesita Parametros (String)
    //      item.setIcon("");
        	item.setUpdate(":formProducto");
            tituloMenu.addElement(item);
        }
        
        model.addElement(tituloMenu);
    }
    public MenuModel getModel() {
        return model;
    }
}