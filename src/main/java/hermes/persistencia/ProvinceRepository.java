package hermes.persistencia;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.Provincia;

@Repository
public interface ProvinceRepository extends PagingAndSortingRepository<Provincia,Integer>{

	public Iterable<Provincia> findAll();
	
}
