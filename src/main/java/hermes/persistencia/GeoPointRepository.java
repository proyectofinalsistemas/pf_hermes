package hermes.persistencia;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.vividsolutions.jts.geom.Point;

import hermes.dominio.PuntoGeografico;

@Repository
public interface GeoPointRepository extends PagingAndSortingRepository<PuntoGeografico,Point>{
	public List<PuntoGeografico> findByPunto(Point punto);
}
