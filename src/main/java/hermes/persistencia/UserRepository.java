package hermes.persistencia;

import hermes.dominio.Usuario;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<Usuario, Integer> {
	
	public List<Usuario> findByUsuarioAndPassword(String usuario,String password);
	public List<Usuario> findByUsuario(String usuario);
	public List<Usuario> findByUsuarioAndPasswordAndHabilitado(String usuario,String password, boolean habilitado);
	public List<Usuario> findByMail(String mail);
}