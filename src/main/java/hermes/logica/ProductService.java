package hermes.logica;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Utilidades.Mensaje;
import Utilidades.Util;
import Utilidades.Validations;
import hermes.dominio.Producto;
import hermes.dominio.TipoProducto;
import hermes.persistencia.ProductRepository;
import hermes.persistencia.ProductTypeRepository;

@Service
public class ProductService {
	@Autowired
	private ProductTypeRepository _productTypeRepository;
	@Autowired
	private ProductRepository _repository;
	
	private Validations validaciones;
	
	public TipoProducto guardarTipoProducto(String name, String descripcion, boolean estado){
		TipoProducto tipoProducto = new TipoProducto(estandarizarPalabras(name), descripcion, estado);
		return actualizarTipoProducto(tipoProducto);
	}
	
	public Producto guardarProducto(String name, String descripcion, BigDecimal precio, String imagen, TipoProducto tipoProducto, boolean estado){
		Producto producto = new Producto(Util.estandarizarPalabras(name), descripcion, precio, imagen, tipoProducto, estado);
		return actualizarProducto(producto);
	}
	
	public List<TipoProducto> listarTodosTipoProducto(){
		return (List<TipoProducto>) _productTypeRepository.findAll();
	}
	
	public List<TipoProducto> listarTodosTipoProductoHabilitados(){
		return _productTypeRepository.findByHabilitado(true);
	}

	public List<Producto> listarTodosProducto(){
		return (List<Producto>) _repository.findAll();
	}
	
	public List<Producto> listarTodosProductosHabilitados(){
		return _repository.findByHabilitadoAndTipoProductoHabilitado(true,true);
	}
	
	public List<Producto> listarProductosPorTipoProducto(String tipoProductoSeleccionado) {
		return _repository.findByTipoProductoNombre(tipoProductoSeleccionado);
	}
	
	public boolean tipoProductoNoExistente(String name){;
		return _productTypeRepository.findByNombre(estandarizarPalabras(name)).isEmpty();
	}
	
	public boolean productoNoExistente(String name, TipoProducto tipoProducto){
		return _repository.findByNombreAndTipoProducto(estandarizarPalabras(name), tipoProducto).isEmpty();
	}

	public TipoProducto actualizarTipoProducto(TipoProducto tipoProducto){
		return _productTypeRepository.save(tipoProducto);
	}
	
	public Producto actualizarProducto(Producto producto){
		return _repository.save(producto);
	}
	
	public TipoProducto obtenerTipoProducto(String nombreTipoProducto){
		return listarTodosTipoProducto().stream()
								.filter(p -> p.getNombre().equalsIgnoreCase(nombreTipoProducto))
								.findFirst()
								.get();
	}

	public void cambiarEstadoProducto(Producto productoSeleccionado) {
		productoSeleccionado.setHabilitado(!productoSeleccionado.getHabilitado());
		actualizarProducto(productoSeleccionado);
	}

	public void cambiarEstadoTipoProducto(TipoProducto tipoProductoSeleccionado) {
		tipoProductoSeleccionado.setHabilitado(!tipoProductoSeleccionado.getHabilitado());
		actualizarTipoProducto(tipoProductoSeleccionado);
	}

	public boolean verificarModificacionTipoProducto(TipoProducto tipoProducto){
		if((_repository.findByTipoProducto(tipoProducto)).isEmpty()) return false;
		else return true;
	}
	
	public List<Producto> verificarTipoProductoDeshabilitado(TipoProducto tipoProducto){ //Este m?todo busca todos los productos relacionados al Tipo de 
		return _repository.findByTipoProducto(tipoProducto); //producto a deshabilitar. A modo de aviso al usuario.
	}

	public boolean productoNoExistente(Producto productoSeleccionado, String nameProducto, String nombreTipoProducto) { 
		List<Producto> productos = _repository.findByNombreAndTipoProducto(estandarizarPalabras(nameProducto), obtenerTipoProducto(nombreTipoProducto));
		if ((productos.isEmpty()) || productos.stream()
									.allMatch(p -> (p.getId().equals(productoSeleccionado.getId())))) return true;
		else return false;
	}
	
	public boolean tipoProductoNoExistente(TipoProducto tipoProductoSeleccionado, String nombreTipoProducto) {
		List<TipoProducto> tipoProductos = _productTypeRepository.findByNombre(estandarizarPalabras(nombreTipoProducto));
		if ((tipoProductos.isEmpty()) || tipoProductos.stream()
				.allMatch(p -> (p.getId().equals(tipoProductoSeleccionado.getId())))) return true;
		else return false;
	}
	
	public String estandarizarPalabras(String palabra){
		return Util.estandarizarPalabras(palabra);
	}

	public Producto obtenerProducto(Integer id) throws SQLException {
		if(!id.equals(null)) return _repository.findByIdAndHabilitado(id,true);
		return null;
	}
	
	public List<Producto> getPrductosPorTipoProducto(Integer id) throws SQLException{
		if(!id.equals(null)) return _repository.findByTipoProductoIdAndHabilitado(id,true);
		return null;
	}

	public TipoProducto obtenerTipoProducto(Integer id) throws SQLException {
		if(!id.equals(null)) return _productTypeRepository.findByIdAndHabilitado(id,true);
		return null;
	}

	public boolean validarProductos(List<Producto> productos) {
		List<Integer> ids = new ArrayList<Integer>();
		productos.stream().forEach(prod-> ids.add(prod.getId()));
		List<Producto> productosBD = new ArrayList<>();
		productos.addAll((Collection<? extends Producto>) _repository.findAll(ids));
		return !productos.stream().filter(prod -> !(productosBD.stream().anyMatch((p->prod.equals(p)))))
								.collect(Collectors.toList())
								.isEmpty();
	}

}
