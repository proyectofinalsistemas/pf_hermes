package hermes.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

import hermes.dominio.Producto;
import hermes.dominio.TipoProducto;
import hermes.dominio.Usuario;
import hermes.logica.OrderService;
import hermes.logica.ProductService;
import lombok.Getter;
import lombok.Setter;

 
@RestController
@Scope("view")
public class DataScrollerView implements Serializable {
    @Autowired
    private ProductService servicioProducto;
    @Autowired
    private OrderService servicioPedido;
    private String message;
    private List<Producto> listaProductos;
    private @Setter @Getter List<Producto> listaProductosFiltrado;
    private @Setter @Getter String tipoProductoSeleccionado;
    
    @PostConstruct
    public void init() {
        listaProductos= servicioProducto.listarTodosProductosHabilitados();
        servicioPedido.setProductos(listaProductos);
        listaProductosFiltrado=listaProductos;
        tipoProductoSeleccionado="Todos Nuestros Productos";
    }

    public List<Producto> getProductos() {
        return listaProductosFiltrado;
    }
    
    public List<Producto> getTodosProductos() {
    	listaProductosFiltrado=listaProductos;
    	setTipoProductoSeleccionado("Todos Nuestros Productos");
        return listaProductosFiltrado;
    }
    
 
    public void setService(ProductService service) {
        this.servicioProducto = service;
    }
    
    public List<Producto> getProductosFiltrados(String nombre) {
    	buscarProductosPorTipoProducto(nombre);
    	tipoProductoSeleccionado=nombre;
    	return listaProductosFiltrado;
    }

	public void buscarProductosPorTipoProducto(String nombre){
    	listaProductosFiltrado = new ArrayList<>();
    	for(Producto producto: listaProductos){
    		if (producto.getTipoProducto().getNombre().equals(nombre) && producto.getHabilitado()==true && producto.getTipoProducto().getHabilitado()==true){
    			listaProductosFiltrado.add(producto);
    		}
    	}
    }
    
    public void agregar(Producto producto) {
        //servicioPedido.agregarACarrito(producto, 1);
    }
    
}