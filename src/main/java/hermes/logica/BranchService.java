package hermes.logica;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import hermes.dominio.Ciudad;
import hermes.dominio.Pedido;
import hermes.dominio.Provincia;
import hermes.dominio.PuntoGeografico;
//import hermes.dominio.Rol;
import hermes.dominio.Sucursal;
import hermes.dominio.TipoProducto;
import hermes.dominio.ZonaInfluencia;
//import hermes.dominio.TipoProducto;
//import hermes.dominio.Usuario;
import hermes.persistencia.CityRepository;
import hermes.persistencia.ProvinceRepository;
import hermes.persistencia.BranchRepository;
import lombok.Getter;
import lombok.Setter;

import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vividsolutions.jts.geom.Point;

import Utilidades.Util;

import Utilidades.Util;

@Service
public class BranchService {
	@Autowired
	private OrderService _servicioPedido;
	@Autowired
	private GeolocationService _servicioGeo;
	@Autowired
	private BranchRepository _repositorio;
	@Autowired
	private CityRepository _repositorioCiudad;
	@Autowired
	private ProvinceRepository _repositorioProvincia;
	
	private @Setter @Getter Sucursal sucursal;
	private Double minDistance;
	
	public void guardarSucursal(String nombre, String telefono, String calle, int numero, String descripcion, Ciudad ciudad, boolean esHabilitada) {
//		boolean hayZona = _servicioGeo.hayZona();
//		if(hayZona){
			//ZonaInfluencia zona = obtenerZonaInfluencia();
			Sucursal sucursal;
			//List<PuntoGeografico> puntos = obtenerPtosGeograficos();	//servicioGeo.validarPtoSucursal();
//			boolean hayPuntos = false;
//			hayPuntos = validarPtosGeograficos();
			
			//if (hayPuntos){
				sucursal = new Sucursal(nombre, telefono, calle, numero, descripcion, ciudad, esHabilitada,obtenerPtoSucursal()); //,obtenerMapa()
//			}else{
//				sucursal = new Sucursal(nombre, telefono, calle, numero, descripcion, ciudad, esHabilitada,obtenerMapa(),obtenerPtoSucursal());
//			}
			setSucursal(sucursal);
//			actualizarPuntoGeografico(sucursal.getZona().getPuntos());
//			actualizarZonaInfluencia(sucursal.getZona());
			actualizarPuntoGeograficoSucursal(sucursal.getGeolocalizacion());
			actualizarSucursal(sucursal);
//			limpiarMapa();
//		} else {
//			Util.addMessageError("Error, no hay zona.", "No se ha demarcado la Zona de Influencia.");
//		}

	}
	
//			if (puntos.isEmpty()){
//				 sucursal = new Sucursal(nombre, telefono, calle, numero, descripcion, ciudad, esHabilitada,obtenerMapa(),obtenerPtoSucursal());
//			}else{
//				sucursal = new Sucursal(nombre, telefono, calle, numero, descripcion, ciudad, esHabilitada,obtenerMapa(),puntos.get(0));
//			}
//			setSucursal(sucursal);
//			actualizarPuntoGeografico(sucursal.getZona().getPuntos());
//			actualizarZonaInfluencia(sucursal.getZona());
//			actualizarPuntoGeograficoSucursal(sucursal.getGeolocalizacion());
//			actualizarSucursal(sucursal);
//			limpiarMapa();
//		} else {
//			Util.addMessageError("Error, no hay zona.", "No se ha demarcado la Zona de Influencia.");
//		}
//	}

//	private boolean validarPtosGeograficos() {
//		boolean hayPtos = _servicioGeo.hayZona();
//		return hayPtos;
//	}

//	private void actualizarZonaInfluencia(ZonaInfluencia zona) {
//		_servicioGeo.guardarZonaInfluencia(zona);
//	}

	private void actualizarPuntoGeograficoSucursal(PuntoGeografico geolocalizacion) {
		_servicioGeo.guardarPuntoGeografico(geolocalizacion);
	}

	private void actualizarPuntoGeografico(List<PuntoGeografico> puntos) {
		_servicioGeo.guardarPuntoGeografico(puntos);
	}

	private LatLng obtenerPtoSucursal() {
		return _servicioGeo.getSucursal();
	}

	private MapModel obtenerMapa() {
		return _servicioGeo.obtenerGeoModel();
	}

	public List<Sucursal> listarTodasSucursales(){
		List<Sucursal> lista = (List<Sucursal>) _repositorio.findAll();
		lista.stream().forEach(s -> s.getGeolocalizacion().setPunto(null));
		return lista;											
	}
	
	public List<Sucursal> listarTodasHabilitadas(){
		List<Sucursal> lista = _repositorio.findByHabilitado(true);
		lista.stream().forEach(s -> s.getGeolocalizacion().setPunto(null));
		return lista;
	}
	
	public List<Ciudad> listarTodasCiudades() {
		return (List<Ciudad>) _repositorioCiudad.findAll();
	}
	
	public List<Ciudad> listarCiudadesPorProvincia(String nombre) {
		return (List<Ciudad>) _repositorioCiudad.findByProvinciaNombre(nombre);
	}
	public List<Provincia> listarTodasProvincias() {
		return (List<Provincia>) _repositorioProvincia.findAll();
	}
	
	public boolean sucursalNoExistente(String nombre, String calle, int numero, Ciudad ciudad) {
		return _repositorio.findByNombreOrCalleAndNumeroAndCiudad(nombre, calle, numero, ciudad).isEmpty();
	}
	
	public boolean sucursalNoExistente(String nombre, String calle, int numero,
										String nombreCiudad, Sucursal sucursalSeleccionada) {
		List<Sucursal> sucursales = _repositorio.findByNombreOrCalleAndNumeroAndCiudad(nombre, calle, numero, obtenerCiudad(nombreCiudad));
		if ((sucursales.isEmpty()) || sucursales.stream()
												.allMatch(s -> (s.getId().equals(sucursalSeleccionada.getId())))) return true;
		else return false;
	}
	
	public Sucursal buscarPorNombre(String nombre){
		return _repositorio.findByNombre(nombre);
	}
	
	public void actualizarSucursal(Sucursal sucursal){
		_repositorio.save(sucursal);
	}

	public void eliminarSucursal(Sucursal sucursal) {
		actualizarSucursal(sucursal);
	}
	
	public Provincia obtenerProvincia(String nombreProvincia) {
		return listarTodasProvincias().stream()
										.filter(p -> p.getNombre().equalsIgnoreCase(nombreProvincia))
										.findFirst()
										.get();
	}
	
	public Ciudad obtenerCiudad(String nombreCiudad){
		return listarTodasCiudades().stream()
				.filter(c -> c.getNombre().equalsIgnoreCase(nombreCiudad))
				.findFirst()
				.get();
	}

	public void cambiarEstado(Sucursal sucursalSeleccionada) {
		sucursalSeleccionada.setHabilitado(!sucursalSeleccionada.getHabilitado());
		actualizarSucursal(sucursalSeleccionada);
	}

	public boolean verificarModificacionSucursal(Sucursal sucursalSeleccionada) {
		return _servicioPedido.verificarModificacionSucursal(sucursalSeleccionada);
	}

	public List<Pedido> verificarSucursalDeshabilitada(Sucursal sucursalSeleccionada) {
		return _servicioPedido.verificarSucursalDeshabilitada(sucursalSeleccionada);
	}

	public void limpiarMapa() {
		_servicioGeo.limpiarMapa();
	}

	public void modoNvaSucursal() {
		_servicioGeo.geocode(); 		
	}

	public boolean nombreNoExistente(String nombre) {
		return ((List<Sucursal>) _repositorio.findByNombre(estandarizarPalabras(nombre))).isEmpty();
	}
	
	public boolean direccionNoExistente(String calle, int numero, Ciudad ciudad) {
		return _repositorio.findByCalleAndNumeroAndCiudad(calle, numero, ciudad).isEmpty();
	}
	
	public String estandarizarPalabras(String palabra){
		return Util.estandarizarPalabras(palabra);
	}

	public Sucursal obtenerSucursalSinGeolocalizacion(Integer id) {
//		try {
		Sucursal suc = _repositorio.findOne(id);
//		} catch (SQLException e) { TODO implementar un SQL Exception.
//			e.printStackTrace();
		suc.getGeolocalizacion().setPunto(null);
		return suc;
	}

	public Sucursal obtenerSucursalParaEnvio(PuntoGeografico pto) {
		List<Sucursal> sucursales = listarTodasHabilitadas();
		this.minDistance=pto.getPunto().distance(sucursales.get(0).getGeolocalizacion().getPunto());
		this.sucursal=sucursales.get(0);
		sucursales.stream()
					.forEach(s -> menorDistancia(pto.getPunto(), s));
		
		return sucursal;
	}

	public void menorDistancia(Point pto, Sucursal suc) {
		Double distance=pto.distance(suc.getGeolocalizacion().getPunto());
		if (distance > this.minDistance)
			this.minDistance=distance;
			this.sucursal = suc;
	}

	public Sucursal obtenerSucursal(Integer id) {
		return  _repositorio.findOne(id);
	}
	
}
