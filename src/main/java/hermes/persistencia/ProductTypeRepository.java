package hermes.persistencia;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.TipoProducto;

@Repository
public interface ProductTypeRepository extends PagingAndSortingRepository<TipoProducto, Integer> {
	
	public List<TipoProducto> findByNombre(String nombre);

	public List<TipoProducto> findByHabilitado(boolean habilitado);

	public TipoProducto findByIdAndHabilitado(Integer id, boolean b);
}
