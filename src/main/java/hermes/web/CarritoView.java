package hermes.web;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

import hermes.dominio.DetallePedido;
import hermes.dominio.Pedido;
import hermes.dominio.Producto;
import hermes.dominio.Sucursal;
import hermes.dominio.TipoEntrega;
import hermes.logica.OrderService;
import lombok.Getter;
import lombok.Setter;

@RestController
@Scope("view")
public class CarritoView implements Serializable {

	@Autowired
	private OrderService servicio;
	private List<Sucursal> sucursalesHabilitadas;
	private String horaActual;
	private String horaEntrega;
	private List<DetallePedido> detalle;
	private Integer cantidad;
	private BigDecimal total;
	private Producto producto;
	private DetallePedido detalleAModificar;
	private DetallePedido detalleAEliminar;
	private String sucursalSeleccionada;
	private @Setter @Getter boolean carrito;
	private TipoEntrega tipo;
	
	@PostConstruct
	public void init() {
		cantidad = 1;
		this.listarSucursalesHabilitadas();
		ordenarSucursales();
		servicio.limpiarVariables();
	}

	public void confirmar(){
		try {
			servicio.setSucursal(buscarSucursal(sucursalSeleccionada));
			
			servicio.confirmar();
			addMessageInformation("Pedido realizado con �xito");
			cerrarDialogo("dlgComprobante");
		} catch (Exception e) {
			//TODO hacer stacktrace de los errores.
			addMessageError("Ha ocurrido un error");
		}
		limpiarVariables();
	}

	public void listarSucursalesHabilitadas() {
		sucursalesHabilitadas = servicio.getSucursalesHabilitadas();
	}
	
	public void ordenarSucursales(){
		Collections.sort(sucursalesHabilitadas,comparator);
	}	
	
	Comparator<Sucursal> comparator = new Comparator<Sucursal>() {
		    public int compare(Sucursal a, Sucursal b) {
		    	int resultado = a.getNombre().compareTo(b.getNombre());
		        return resultado;
		    }
		};	
	
	public void agregarACarrito() {
		//servicio.agregarACarrito(producto, cantidad);
		detalle = servicio.getDetalle();
		getTotal();
		setCarrito(true);
		cerrarDialogo("dlgDetalle");
	}
	public void modificarCarrito() {
		servicio.modificarCarrito(detalleAModificar);
	}
	
	public void eliminarProducto(){
		servicio.eliminarProducto(detalleAEliminar);
	}

	public void productoSeleccionado(Producto p) {
		producto = p;
		cantidad = 1;
		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("PF('dlgDetalle').show()");
	}

	public void limpiarVariables() {
		detalle.clear();
		cantidad = 0;
		total = total.ZERO;
		producto = null;
		detalleAModificar = null;
		sucursalSeleccionada = "";
		carrito=false;
	}

	public boolean detalleVacio() {
		return servicio.getDetalle().isEmpty();
	}

	public Sucursal buscarSucursal(String nombre) {
		Sucursal s = null;
		for (Sucursal sucursal : sucursalesHabilitadas) {
			if (sucursal.getNombre().equals(nombre))
				s = sucursal;
		}
		return s;
	}
	
	public void revisarConfirmacion(){
		if (!detalleVacio()) {
			try {
				System.out.println("Valido Sucursales");
				System.out.println(sucursalesHabilitadas.size());
				listarSucursalesHabilitadas();
				System.out.println(sucursalesHabilitadas.size());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlgComprobante').show()");	
			} catch (Exception e) {
				addMessageError("Ha ocurrido un error");
			}
		} else {
			addMessageInformation("Agregue algún producto a la lista del pedido");
		}
	}
	
	public void cerrarDialogo(String dialog) {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('" + dialog + "').hide()");
	}
	
	public BigDecimal getTotal() {
		total = servicio.getTotal();
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<Sucursal> getSucursalesHabilitadas() {
		return sucursalesHabilitadas;
	}

	public void setSucursalesHabilitadas(List<Sucursal> sucursalesHabilitadas) {
		this.sucursalesHabilitadas = sucursalesHabilitadas;
	}

	public String getHoraActual() {
		return horaActual;
	}

	public void setHoraActual(String horaActual) {
		this.horaActual = horaActual;
	}

	public String getHoraEntrega() {
		return horaEntrega;
	}

	public void setHoraEntrega(String horaEntrega) {
		this.horaEntrega = horaEntrega;
	}

	public List<DetallePedido> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<DetallePedido> detalle) {
		this.detalle = detalle;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto productoSeleccionado) {
		this.producto = productoSeleccionado;
	}

	public DetallePedido getDetalleAModificar() {
		return detalleAModificar;
	}

	public void setDetalleAModificar(DetallePedido detalleAModificar) {
		this.detalleAModificar = detalleAModificar;
	}

	public String getSucursalSeleccionada() {
		return sucursalSeleccionada;
	}

	public void setSucursalSeleccionada(String sucursalSeleccionada) {
		this.sucursalSeleccionada = sucursalSeleccionada;
		servicio.setSucursal(buscarSucursal(sucursalSeleccionada));
	}

	public DetallePedido getDetalleAEliminar() {
		return detalleAEliminar;
	}

	public void setDetalleAEliminar(DetallePedido detalleAEliminar) {
		servicio.eliminarProducto(detalleAEliminar);
		this.detalleAEliminar = detalleAEliminar;
		carritoVacio();
	}

	public void carritoVacio() {
		if (detalleVacio()){
			setCarrito(false);
		}
	}

	public void addMessageInformation(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void addMessageError(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	public void addMessageError() {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "EROR", null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
}
