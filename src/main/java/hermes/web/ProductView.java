package hermes.web;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.component.selectonebutton.SelectOneButton;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

import Utilidades.Util;
import antlr.debug.Event;
import hermes.dominio.ImagenProducto;
import hermes.dominio.Pedido;
import hermes.dominio.Producto;
import hermes.dominio.Rol;
import hermes.dominio.TipoProducto;
import hermes.logica.OrderService;
import hermes.logica.ProductService;
import lombok.Getter;
import lombok.Setter;

@RestController
@Scope("view")
public class ProductView {

	@Autowired
	private ProductService servicio;
	@Autowired
	private OrderService servicioPedido;
	
	private String nombreTipoProducto;
	private String descripcionTipoProducto;
	private boolean estadoTipoProducto;
	private String nombreProducto;
	private String descripcionProducto;
	private BigDecimal precio;
	private boolean estadoProducto;
	private @Getter @Setter List<Producto> productos;
	private List<TipoProducto> tipoProductos;
	private Producto productoSeleccionado;
	private TipoProducto tipoProductoSeleccionado;
	private String tipoProductoSeleccionadoCombo;
	private boolean noHaySeleccionado;
	private List<Producto> filtroProductos;
	private @Getter @Setter List<TipoProducto> filtroTipoProductos;
	private List<Producto> productosAfectados;
	private @Getter @Setter List<Pedido> pedidosAfectados;
	private TipoProducto tipoProducto;
	private String urlImagen;//url

	@PostConstruct
	public void init() {
		this.cargarProducto();
		this.cargarTipoProducto();
		this.noHaySeleccionado = true;
	}

	public void saveProducto() {
		tipoProducto = servicio.obtenerTipoProducto(tipoProductoSeleccionadoCombo);
		if (this.validarProductoNoExistente()) {
			Producto prod = servicio.guardarProducto(getNombreProducto(), getDescripcionProducto(), getPrecio(), getUrlImagen(), tipoProducto,
					getEstadoProducto());
			addMessage("Guardado con �xito", "Datos guardados");
			cerrarDialogo("dlgNuevoProducto");
			agregarProducto(prod);
		} else {
			addMessage("Producto ya existente", "");
		}
		deshabilitarBotones();
	}
	
	public void saveTipoProducto() {
		if (this.validarTipoProductoNoExistente()) {
			TipoProducto tipo = servicio.guardarTipoProducto(nombreTipoProducto, descripcionTipoProducto, estadoTipoProducto);
			addMessage("Guardado con �xito", "Datos guardados");
			cerrarDialogo("dlgNuevoTipoProducto");
			agregarTipoProducto(tipo);
		} else {
			addMessage("Tipo de Producto ya existente", "");
		}
		deshabilitarBotones();
	}
	
	// MODIFICACI�N
	public void updateProducto() {
		if(this.validarProductoNoExistente(productoSeleccionado)){
			productoSeleccionado.setNombre(servicio.estandarizarPalabras(nombreProducto));
			productoSeleccionado.setDescripcion(descripcionProducto);
			productoSeleccionado.setPrecio(precio);
			tipoProducto = servicio.obtenerTipoProducto(tipoProductoSeleccionadoCombo);
			productoSeleccionado.setImagen(urlImagen);
			productoSeleccionado.setTipoProducto(tipoProducto);
			productoSeleccionado.setHabilitado(getEstadoProducto());
			servicio.actualizarProducto(productoSeleccionado);
			deshabilitarBotones();
			addMessage("Guardado con �xito", "Datos guardados");
			cerrarDialogo("dlgModificarProducto");
		} else {
			addMessage("Producto ya existente", "");
		} 
	}

	public void updateTipoProducto() {
		if (this.validarTipoProductoNoExistente(tipoProductoSeleccionado)){
			tipoProductoSeleccionado.setNombre(servicio.estandarizarPalabras(nombreTipoProducto));
			tipoProductoSeleccionado.setDescripcion(descripcionTipoProducto);
			tipoProductoSeleccionado.setHabilitado(estadoTipoProducto);
			servicio.actualizarTipoProducto(tipoProductoSeleccionado);
			addMessage("Guardado con �xito", "Datos guardados");
			deshabilitarBotones();
			cerrarDialogo("dlgModificarTipoProducto");
		} else {
			addMessage("Tipo de Producto ya existente", "");
		}
	}

	public void modificarProducto() {
		setNombreProducto(productoSeleccionado.getNombre());
		setDescripcionProducto(productoSeleccionado.getDescripcion());
		setPrecio(productoSeleccionado.getPrecio());
		setUrlImagen(productoSeleccionado.getImagen());
		setTipoProductoSeleccionadoCombo(productoSeleccionado.getTipoProducto().getNombre());
		setEstadoProducto(productoSeleccionado.getHabilitado());
	}

	public void modificarTipoProducto() {
		setNombreTipoProducto(tipoProductoSeleccionado.getNombre());
		setDescripcionTipoProducto(tipoProductoSeleccionado.getDescripcion());
		setEstadoTipoProducto(tipoProductoSeleccionado.getHabilitado());
	}
	
	// ESTADOS
	public void cambiarEstadoProducto() {
		servicio.cambiarEstadoProducto(productoSeleccionado);
		addMessage("Estado cambiado con �xito", "Acci�n realizada con �xito.");
		deshabilitarBotones();
	}
	
	public void cambiarEstadoTipoProducto() {
		servicio.cambiarEstadoTipoProducto(tipoProductoSeleccionado);
		addMessage("Estado cambiado con �xito", "Acci�n realizada con �xito.");
		deshabilitarBotones();
	}

	// CARGAR LISTAS 
	public void agregarProducto(Producto producto) {
		this.productos.add(producto);
	}
	public void agregarTipoProducto(TipoProducto tipo) {
		this.tipoProductos.add(tipo);
	}
	
	public void cargarProducto() {
		productos = servicio.listarTodosProducto();
	}	
	public void cargarTipoProducto() {
		tipoProductos = servicio.listarTodosTipoProducto();
	}
	
	// VALIDACIONES
	public void validarModificacionProducto() {
		if((productoSeleccionado.getNombre().equals(nombreProducto))&&(productoSeleccionado.getDescripcion().equals(descripcionProducto))&&
				(productoSeleccionado.getTipoProducto().getNombre().equals(tipoProductoSeleccionadoCombo))&&(productoSeleccionado.getPrecio().equals(precio))&&
				(productoSeleccionado.getImagen().equals(getUrlImagen()))&&(productoSeleccionado.getHabilitado()==estadoProducto)){
			addMessage("No Guardado", "No modific� ning�n dato.");
		} else{
			if (servicioPedido.verificarModificacionProducto(productoSeleccionado)) {
				this.pedidosAfectados = servicioPedido.verificarProductoDeshabilitado(productoSeleccionado);
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlgVerificacionModificacion').show()");
			} else {
				updateProducto();
			}
		}
	}
	public void validarModificacionTipoProducto() {
		if((tipoProductoSeleccionado.getNombre().equals(nombreTipoProducto))&&(tipoProductoSeleccionado.getDescripcion().equals(descripcionTipoProducto))&&(tipoProductoSeleccionado.getHabilitado()==estadoTipoProducto)){
			addMessage("No Guardado", "No modific� ning�n dato.");
		} else{
			if (servicio.verificarModificacionTipoProducto(tipoProductoSeleccionado)) {
				this.productosAfectados = servicio.verificarTipoProductoDeshabilitado(tipoProductoSeleccionado);
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlgVerificacionModificacion').show()");
			} else {
				updateTipoProducto();
			}
		}
	}
	
	public void verificarDeshabilitacionP() {
		if (productoSeleccionado.getHabilitado()) {
			if (servicioPedido.verificarModificacionProducto(productoSeleccionado)) {
				this.pedidosAfectados = servicioPedido.verificarProductoDeshabilitado(productoSeleccionado);
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlgVerificacionDeshabilitacion').show()");
			} else {
				cambiarEstadoProducto();
			}
		} else {
			cambiarEstadoProducto();
		}
	}
	public void verificarDeshabilitacionTP() {
		if (tipoProductoSeleccionado.getHabilitado()) {
			if (servicio.verificarModificacionTipoProducto(tipoProductoSeleccionado)) {
				this.productosAfectados = servicio.verificarTipoProductoDeshabilitado(tipoProductoSeleccionado);
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlgVerificacionDeshabilitacion').show()");
			} else {
				cambiarEstadoTipoProducto();
			}
		} else {
			cambiarEstadoTipoProducto();
		}
	}

	private boolean validarProductoNoExistente() {
		return servicio.productoNoExistente(nombreProducto, tipoProducto);
	}
	private boolean validarProductoNoExistente(Producto productoSeleccionado) {
		return servicio.productoNoExistente(productoSeleccionado, nombreProducto, tipoProductoSeleccionadoCombo);
	}
	private boolean validarTipoProductoNoExistente() {
		return servicio.tipoProductoNoExistente(nombreTipoProducto);
	}
	private boolean validarTipoProductoNoExistente(TipoProducto tipoProductoSeleccionado) {
		return servicio.tipoProductoNoExistente(tipoProductoSeleccionado, nombreTipoProducto);
	}
	
	// M�TODOS DE EVENTOS
	public void addMessage(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void onRowSelect(SelectEvent event) {
		this.noHaySeleccionado = false;
	}

	public void rowDblSelectProducto() {
		modificarProducto();
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('dlgModificarProducto').show()");
	}

	public void rowDblSelectTipoProducto() {
		modificarTipoProducto();
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('dlgModificarTipoProducto').show()");
	}

	public void cerrarDialogo(String dialog) {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('" + dialog + "').hide()");
	}

	// M�TODOS DE CONFIGURACI�N DE PANTALLAS
	public void deshabilitarBotones() {
		this.productoSeleccionado = null;
		this.tipoProductoSeleccionado = null;
		this.setNoHaySeleccionado(true);
	}

	public void vaciarCampos() {
		setNombreProducto("");
		setDescripcionProducto("");
		setPrecio(null);
		setUrlImagen("");
		setEstadoProducto(true);
		setNombreTipoProducto("");
		setDescripcionTipoProducto("");
		setTipoProductoSeleccionadoCombo("");
		setEstadoTipoProducto(true);
	}
	
	// OTROS M�TODOS
	public String removeAcentos(String s) {
        if (s == null) {
            return "";
        }
        String semAcentos = s.toLowerCase();
        semAcentos = semAcentos.replaceAll("[áàâãä]", "a");
        semAcentos = semAcentos.replaceAll("[éèêë]", "e");
        semAcentos = semAcentos.replaceAll("[íìîï]", "i");
        semAcentos = semAcentos.replaceAll("[óòôõö]", "o");
        semAcentos = semAcentos.replaceAll("[úùûü]", "u");
        semAcentos = semAcentos.replaceAll("ç", "c");
        semAcentos = semAcentos.replaceAll("ñ", "n");
        return semAcentos;
	}
	
	// GETTERS Y SETTERS
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public String getDescripcionProducto() {
		return descripcionProducto;
	}
	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public BigDecimal getPrecio() {
		return precio;
	}
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public Producto getProductoSeleccionado() {
		return productoSeleccionado;
	}
	public void setProductoSeleccionado(Producto productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public String getNombreTipoProducto() {
		return nombreTipoProducto;
	}
	public void setNombreTipoProducto(String nombreTipoProducto) {
		this.nombreTipoProducto = nombreTipoProducto;
	}

	public String getDescripcionTipoProducto() {
		return descripcionTipoProducto;
	}
	public void setDescripcionTipoProducto(String descripcionTipoProducto) {
		this.descripcionTipoProducto = descripcionTipoProducto;
	}

	public List<TipoProducto> getTipoProductos() {
		return tipoProductos;
	}

	public void setTipoProductoSeleccionado(TipoProducto tipoProductoSeleccionado) {
		this.tipoProductoSeleccionado = tipoProductoSeleccionado;
	}
	public TipoProducto getTipoProductoSeleccionado() {
		return tipoProductoSeleccionado;
	}

	public void setNoHaySeleccionado(boolean seleccion) {
		noHaySeleccionado = seleccion;
	}
	public boolean getNoHaySeleccionado() {
		return noHaySeleccionado;
	}

	public String getTipoProductoSeleccionadoCombo() {
		return tipoProductoSeleccionadoCombo;
	}
	public void setTipoProductoSeleccionadoCombo(String tipoProductoSeleccionadoCombo) {
		this.tipoProductoSeleccionadoCombo = tipoProductoSeleccionadoCombo;
	}

	public List<Producto> getFiltroProductos() {
		return filtroProductos;
	}
	public void setFiltroProductos(List<Producto> filtroProductos) {
		this.filtroProductos = filtroProductos;
	}

	public boolean getEstadoTipoProducto() {
		return estadoTipoProducto;
	}
	public void setEstadoTipoProducto(boolean estadoTipoProducto) {
		this.estadoTipoProducto = estadoTipoProducto;
	}

	public boolean getEstadoProducto() {
		return estadoProducto;
	}
	public void setEstadoProducto(String estadoProducto) {
		if (estadoProducto == "Habilitado") this.estadoProducto = true;
		else this.estadoProducto = false;
	}

	public void setEstadoProducto(boolean estadoProducto) {
		this.estadoProducto = estadoProducto;
	}

	public List<Producto> getProductosAfectados() {
		return productosAfectados;
	}
	public void setProductosAfectados(List<Producto> productosAfectados) {
		this.productosAfectados = productosAfectados;
	}

	public String getUrlImagen() {
		return urlImagen;
	}
	public void setUrlImagen(String urlImagen) {
		this.urlImagen = urlImagen;
	}
	
//	public void subirArchivo(FileUploadEvent event){
//  try{
//  	UploadedFile file = event.getFile();
//  	if(file != null && file.getContents() != null && file.getContents().length > 0 && file.getFileName() != null) {
//  		this.imagenFile = file;
//  	}
//  	imagenBit=event.getFile().getContents();
//  	imagen=Util.guardarFichero(imagenBit, event.getFile().getFileName());
//      System.out.println(imagen);
//  }catch (Exception e){
//      FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"El nombre producto existe en ese rubro", null));
//  }
//	
//	System.out.println(imagenFile.getFileName());
//}

//public void obtenerUrlImagen() {
//	String url="";
//	String formatoImagen = imagenFile.getFileName().toLowerCase();
//	int cortar = formatoImagen.indexOf(".");
//  formatoImagen = formatoImagen.substring(cortar, formatoImagen.length());
//  
//  if ((formatoImagen.equals(".jpg") || formatoImagen.equals(".jpeg"))) {
//      try {
//          imagenBit = imagenFile.getContents();
//          url = Util.guardarFichero(imagenBit, imagenFile.getFileName());
//          setImagen(url);
//      } catch (Exception e){
//      	addMessage("Problemas al cargar el archivo","");
//      	
//      }
//	
//  } else {
//		addMessage("Formato no aceptado.", "");
//	}
//  
//  
//}

//	public void handleFileUpload(FileUploadEvent event) {
//	ImagenProducto imagenProducto = new ImagenProducto();
//	imagenProducto.setContenido(event.getFile().getContents());
//	imagenProducto.setTipo(event.getFile().getContentType());
//	FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
//	FacesContext.getCurrentInstance().addMessage(null, message);
//}
}
