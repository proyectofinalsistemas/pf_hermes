package hermes.dominio;

import javax.persistence.Entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity @EqualsAndHashCode(callSuper=true)
public class Permiso extends ObjetoPersistente {
	private @NonNull String nombre;
}
