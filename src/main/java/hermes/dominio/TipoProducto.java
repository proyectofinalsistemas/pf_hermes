package hermes.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity
public class TipoProducto{
	@Id @GeneratedValue
	private Integer id;
	protected boolean habilitado;

	@NotNull
	private @NonNull String nombre;
	private String descripcion;
	
	public TipoProducto(String nombre, String descripcion, boolean estado) {
		this.setNombre(nombre);
		this.setDescripcion(descripcion);
		this.setHabilitado(estado);
	}
	
	public String toString(){
		return nombre;
	}

	public Integer getId() {
		return id;
	}
	
	public void setHabilitado(boolean bool){
		this.habilitado= bool;
	}
	public boolean getHabilitado(){
			return this.habilitado;
	}
}
