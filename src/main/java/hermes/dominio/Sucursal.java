package hermes.dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity 
public class Sucursal{
	@Id @GeneratedValue
	private Integer id;
	protected boolean habilitado;
	@NotNull
	private @NonNull String nombre;
	@NotNull
	private @NonNull String telefono;
	@NotNull
	private @NonNull String calle;
	@NotNull
	private int numero;
	private String descripcion;
	@ManyToOne @NotNull
	private Ciudad ciudad;
//	@OneToOne @NotNull @NonNull
//	private ZonaInfluencia zona; 				//Guardo la zona de influencia en la que va a abarcar los pedidos una sucursal.
	@OneToOne @NotNull @NonNull
	private PuntoGeografico geolocalizacion;	//Guardo el punto geográfico de la sucursal.
	
	public Sucursal(String nombre, String telefono, String calle, int numero, String descripcion, Ciudad ciudad, boolean esHabilitada, LatLng geo) { //MapModel map,
		this.setNombre(nombre);
		this.setTelefono(telefono);
		this.setCalle(calle);
		this.setNumero(numero);
		this.setDescripcion(descripcion);
		this.setCiudad(ciudad);
		this.setHabilitado(esHabilitada);
		//setZona(createZona(map));
		setGeolocalizacion(crearGeolocalizacion(geo));
	}
	
	public Sucursal(String nombre, String telefono, String calle, int numero, String descripcion, Ciudad ciudad, boolean esHabilitada, PuntoGeografico punto) {//MapModel map, 
		this.setNombre(nombre);
		this.setTelefono(telefono);
		this.setCalle(calle);
		this.setNumero(numero);
		this.setDescripcion(descripcion);
		this.setCiudad(ciudad);
		this.setHabilitado(esHabilitada);
		//setZona(createZona(map));
		setGeolocalizacion(punto);
	}
	
//	public ZonaInfluencia createZona(MapModel map) {
//		ZonaInfluencia zona = new ZonaInfluencia(map, this.getNombre());
//		return zona;
//	}

	private PuntoGeografico crearGeolocalizacion(LatLng geo) {
		
		PuntoGeografico punto = new PuntoGeografico(geo);
		return punto;
	}

	public Integer getId() {
		return id;
	}
	
	public void setHabilitado(boolean bool){
		this.habilitado= bool;
	}

	@Override
	public String toString(){
		return nombre;
	}
	
	public boolean getHabilitado() {
			return this.habilitado;
	}
}
