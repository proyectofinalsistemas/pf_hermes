package hermes.dominio;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity 
public class Ciudad {
	@Id
	private Integer id;
	private @NonNull String nombre;
	private String descripcion;
	@ManyToOne
	private Provincia provincia;

@Override
public String toString(){
	return nombre;
}

}