package hermes.persistencia;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.DetallePedido;

@Repository
public interface OrderDetailRepository extends PagingAndSortingRepository<DetallePedido, Long>{

}
