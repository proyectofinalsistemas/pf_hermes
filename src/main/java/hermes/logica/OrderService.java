package hermes.logica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import Utilidades.Mensaje;
import Utilidades.Respuesta;
import Utilidades.Util;
import Utilidades.Validations;
import hermes.dominio.Cancelacion;
import hermes.dominio.DetallePedido;
import hermes.dominio.EstadoPedido;
import hermes.dominio.MotivosCancelacion;
import hermes.dominio.Pedido;
import hermes.dominio.Producto;
import hermes.dominio.PuntoGeografico;
import hermes.dominio.Sucursal;
import hermes.dominio.TipoEntrega;
import hermes.dominio.TipoPago;
import hermes.persistencia.OrderDetailRepository;
import hermes.persistencia.OrderStatusRepository;
import hermes.persistencia.OrderRepository;
import hermes.web.CarritoView;

@Service
public class OrderService {

	@Autowired
	private BranchService _branchService;
	@Autowired
	private UserService _userService;
	@Autowired
	private SupportService _supportService;
	@Autowired
	private ProductService _productService;
	@Autowired
	private OrderRepository _repository;
	@Autowired
	private OrderDetailRepository _detailRepository;
	@Autowired
	private OrderStatusRepository _statusRepository;
	private CarritoView carrito;
	private Sucursal sucursal;
	private List<Sucursal> sucursalesHabilitadas;
	private List<Producto> productos;
	private List<DetallePedido> detalle;
	private BigDecimal total;
	private Date fechaActual;
	private Date horaActual;
	private Date fechaEntrega;
	private Date horaEntrega;
	private Set<Pedido> pedidosCocina;
	private EstadoPedido estado;
	private Validations validaciones;
	
	private String sucursalDelUsuario;

	@PostConstruct
	public void init() {
		sucursalesHabilitadas = new ArrayList<>();
		//this.listarSucursales();
		productos = new ArrayList<>();
		detalle = new ArrayList<>();
		pedidosCocina = new HashSet<>();
	}

	public List<MotivosCancelacion> listarMotivosCancelacion() {
		return _supportService.listarTodosMotivosCancelacion();
	}

	public List<Sucursal> listarSucursales() {
		return _branchService.listarTodasSucursales();
		}

	public void listarSucursalesHabilitadas() {
		sucursalesHabilitadas.clear();
		sucursalesHabilitadas = _branchService.listarTodasHabilitadas();
	}
	
	public Date obtenerFechaActual() {
		Calendar fecha = Calendar.getInstance();
		return fecha.getTime();
	}

	public String obtenerHoraActual() {
		Calendar horaActual = new GregorianCalendar();
		int hora, minutos, segundos;
		this.horaActual = horaActual.getTime();
		hora = horaActual.get(Calendar.HOUR_OF_DAY);
		minutos = horaActual.get(Calendar.MINUTE);
		segundos = horaActual.get(Calendar.SECOND);
		return hora + ":" + minutos + ":" + segundos;
	}

//	public boolean validarHorario(GregorianCalendar hora) {
//		return false;
//	}

	public Date obtenerFechaEntrega() {
		Calendar fecha = new GregorianCalendar();
		fecha.add(Calendar.MINUTE, 30);
//		SimpleDateFormat hora = new SimpleDateFormat("H:mm");
//		if((hora.format(fecha.getTime()))>("19:59")&&(hora.format(fecha.getTime()))<("00:00")){
//			
//		}
		return fecha.getTime();
	}
	
	// Deber�a calcularlo seg�n la hora en que se registra el pedido
	public String obtenerHoraEntrega() {
		Calendar horaEntrega = new GregorianCalendar();
		horaEntrega.add(Calendar.MINUTE, 30);
		int hora, minutos, segundos;
		this.horaEntrega = horaEntrega.getTime();
		hora = horaEntrega.get(Calendar.HOUR_OF_DAY);
		minutos = horaEntrega.get(Calendar.MINUTE);
		segundos = horaEntrega.get(Calendar.SECOND);
		return hora + ":" + minutos + ":" + segundos;
	}


//	public void agregarACarrito(Producto producto, int cantidad) {
//		boolean noExiste = true;
//		int index = 0;
//		for (DetallePedido detalles : detalle) {
//			if (detalles.getNombreProducto().equalsIgnoreCase(producto.getNombre())
//					&& detalles.getNombreTipoProducto().equalsIgnoreCase(producto.getTipoProducto().getNombre())) {
//				noExiste = false;
//				int cantidadTotal = detalle.get(index).getCantidad() + cantidad;
//				detalle.get(index).setCantidad(cantidadTotal);
//				BigDecimal subtotal = calcularSubtotal(producto, cantidadTotal);
//				detalle.get(index).setSubtotal(subtotal);
//			}
//			index++;
//		}
//		if (noExiste) {
//			BigDecimal subtotal = calcularSubtotal(producto, cantidad);
//			detalle.add(new DetallePedido(producto, subtotal, cantidad, buscarEstadoInicial(), producto.getNombre(),
//					producto.getTipoProducto().getNombre(), producto.getPrecio()));
//		}
//	}

	public Producto obtenerProducto(String nombre) {
		try {
			return getProductos().stream()
					.filter(p -> p.getNombre().equalsIgnoreCase(nombre))
					.findFirst()
					.get();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void modificarCarrito(DetallePedido detalle) {
		BigDecimal subtotal = calcularSubtotal(detalle.getPrecioProducto(), detalle.getCantidad());
		detalle.setSubtotal(subtotal);
	}

	public void eliminarProducto(DetallePedido detalle) {
		this.detalle.remove(detalle);
	}

	public BigDecimal calcularSubtotal(BigDecimal precio, int cantidad) {
		return precio.multiply(new BigDecimal(cantidad));
	}

	public BigDecimal calcularTotal(List<DetallePedido> detalles) {
		BigDecimal total = detalles.stream()
				.filter(d -> !(d.getEstado().getNombre().equals("Cancelado")))
				.map(DetallePedido::getSubtotal)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		return total;
	}

	public boolean validarCantidad(int cantidad) {
		if (cantidad > 10) return false;
		else return true;
	}

//	public void confirmarPedido() {
//		confirmar();
//		carrito.cerrarDialogo("dlgComprobante");
//		carrito.limpiarVariables();
//	}

	public void confirmar() {
//		Pedido pedido = new Pedido(obtenerFechaActual(), obtenerFechaActual(), obtenerFechaEntrega(),
//				obtenerFechaEntrega(), detalle, total, estadoInicial, sucursal, tipoEntrega, puntoEntrega);
//		repositorioDetallePedido.save(detalle);
//		repositorioPedido.save(pedido);
//		limpiarVariables();
	}

	public void limpiarVariables() {
		detalle.clear();
		total = total.ZERO;
	}

	public List<Sucursal> getSucursalesHabilitadas() {
		return _branchService.listarTodasHabilitadas();
	}

	
	public List<Producto> getProductos() {
		return productos;
	}
	
	public List<Pedido> listarTodosPedidosHistoricos() {
		return (List<Pedido>) _repository.findAll();
	}

	public List<Pedido> listarTodosPedidosActuales() {
		return _repository.findByFechaPedido(obtenerFechaActual());
	}


	public void cancelar(Pedido pedido, String motivoCancelacionSeleccionado, String descripcion) { //Cancelar Pedido
		estado = _statusRepository.findByNombreIgnoreCase("Cancelado").get(0);
		MotivosCancelacion motivo = obtenerMotivoCancelacion(motivoCancelacionSeleccionado);
		pedido.setEstado(estado);
		for (DetallePedido detalle : pedido.getDetalle()){
			detalle.setEstado(estado);
			Cancelacion cancelacion = new Cancelacion(descripcion, pedido, detalle, motivo);
			_supportService.guardarCancelacion(cancelacion);
		}
		_detailRepository.save(pedido.getDetalle());
		_repository.save(pedido);
	}
	
	//Cancelar Pedido
	public Respuesta cancelar(Cancelacion cancelacion) { 										
		Respuesta rta = null;
		try {
			this.validaciones = new Validations();
			verificarPedidoCancelacion(cancelacion.getPedido());
			verificarEstadoCancelacion(cancelacion);
			verificarMotivo(cancelacion.getMotivo());
			EstadoPedido estado = _statusRepository.findByNombreIgnoreCase("Cancelado").get(0);
			cancelacion.getPedido().setEstado(estado);
			if(validaciones.isValid()) {
				if (cancelacion.getDetalle()==null) {
					for (DetallePedido detalle : cancelacion.getPedido().getDetalle()){
						detalle.setEstado(estado);
						Cancelacion cancel = new Cancelacion(cancelacion.getDescripcion(), cancelacion.getPedido(), detalle, cancelacion.getMotivo());
						_supportService.guardarCancelacion(cancel);
					}
				} else {
					Cancelacion cancel = new Cancelacion(cancelacion.getDescripcion(), cancelacion.getPedido(), cancelacion.getDetalle(), cancelacion.getMotivo());
					_supportService.guardarCancelacion(cancel);
				}
				this.saveDetallePedido(cancelacion.getDetalle());
				this.savePedido(cancelacion.getPedido());
				rta = new Respuesta("201", "Pedido cancelado con �xito!", (Object) cancelacion);
			} else {
	    		rta = new Respuesta("404", validaciones.getMensajes());
	    	}
		}catch(Exception e){
			e.printStackTrace();
    		rta = new Respuesta("599", "Error al guardar el Pedido. "+e.toString());
		}
		return rta;
	}
	
	private void verificarPedidoCancelacion(Pedido pedido) {
		if(pedido == null || pedido.getDetalle()==null) this.validaciones.getMensajes()
												.add(new Mensaje("ERROR","ERROR14: Debe ser expecificado un pedido o �tem de pedido a ser cancelado."));
	}

	public void verificarMotivo(MotivosCancelacion motivo) {
		if(motivo == null || !listarMotivosCancelacion().stream().anyMatch(m-> m.equals(motivo))) this.validaciones.getMensajes()
																			.add(new Mensaje("ERROR","ERROR17: El Motivo de Cancelaci�n no es v�lido."));
	}

	public void verificarEstadoCancelacion(Cancelacion cancelacion) {
		if(cancelacion.getPedido().getEstado().getNombre().equalsIgnoreCase("Cancelado") 
				|| cancelacion.getDetalle().getEstado().getNombre().equalsIgnoreCase("Cancelado")) this.validaciones.getMensajes()
																				.add(new Mensaje("ERROR","ERROR15: El Pedido y/o �tem ya se encuentra Cancelado"));
		else if(!cancelacion.getPedido().getEstado().getNombre().equalsIgnoreCase("En Espera")) this.validaciones.getMensajes()
					.add(new Mensaje("ERROR","ERROR16: El estado actual del Pedido no permite ser modificado, cont�ctese con la sucursal a cargo de su pedido: "+cancelacion.getPedido().getSucursal().getTelefono()));
	}

	public MotivosCancelacion obtenerMotivoCancelacion(String motivoCancelacionSeleccionado) {
		MotivosCancelacion motivo = null;
		for (MotivosCancelacion motivoc : this.listarMotivosCancelacion()) {
			if (motivoCancelacionSeleccionado.equals(motivoc.getDescripcion())) {
				motivo = motivoc;
			}
		}
		return motivo;
	}

	
	
	public List<Pedido> listarTodosPedidosHistoricos(Date fechaDesde, Date fechaHasta) {
		return (List<Pedido>) _repository.findByFechaPedidoBetween(fechaDesde, fechaHasta);
	}

	private EstadoPedido buscarEstadoInicial() {
		return _statusRepository.findByNombreIgnoreCase("En Espera").get(0);
	}

	public List<EstadoPedido> listarEstados() {
		return (List<EstadoPedido>) _statusRepository.findAll();
	}

	public boolean verificarModificacionSucursal(Sucursal sucursal) {
		List<Pedido> pedidos = _repository.findBySucursalNombre(sucursal.getNombre());
		if (pedidos.isEmpty()) return false;
		else return true;	
	}

	public List<Pedido> verificarSucursalDeshabilitada(Sucursal sucursal) {
		List<Pedido> lista = _repository.findByEstadoNombreOrEstadoNombre("En Espera", "En Preparaci�n");
		return lista.stream()
					.filter(pe -> pe.getSucursal().getNombre().equalsIgnoreCase(sucursal.getNombre()))
					.collect(Collectors.toList());
	}

	public boolean verificarModificacionProducto(Producto producto) {
		List<Pedido> pedidos = _repository.findByDetalleNombreProductoAndDetalleNombreTipoProducto(
				producto.getNombre(), producto.getTipoProducto().getNombre());
		return (!pedidos.isEmpty());
	}

	public List<Pedido> verificarProductoDeshabilitado(Producto producto) {
		List<Pedido> lista = _repository.findByEstadoNombreOrEstadoNombre("En Espera", "En Preparaci�n");
		return lista.stream()
				.filter(pe -> pe.getDetalle().stream()
						.filter(de -> de.getNombreProducto().equalsIgnoreCase(producto.getNombre()) && 
									  de.getNombreTipoProducto().equalsIgnoreCase(producto.getTipoProducto().getNombre()))
						.findFirst()
						.isPresent()
						)
				.collect(Collectors.toList());
	 }

	public boolean esEnEspera(Pedido pedidosActualSeleccionado) {
		return (pedidosActualSeleccionado.getEstado().getNombre().equalsIgnoreCase("En Espera"));
	}

	public Set<Pedido> pedidosCocina() {
		pedidosCocina.clear();
		setSucursalDeUsuario();
		for (Pedido pedido : listarTodosPedidosActuales(sucursalDelUsuario)) {
			if (pedido.getEstado().getNombre().equalsIgnoreCase("En Espera") || (pedido.getEstado().getNombre().equalsIgnoreCase("En Preparaci�n"))){
				pedidosCocina.add(pedido);
//			Este m�todo no se si debe o no quedar. Es una diferencia que ten�a con la versi�n de 2017. Con el testing deber�amos definir si lo dejamos o lo quitamos.
//			for (DetallePedido detalle : pedido.getDetalle()){
//				if(detalle.getEstado().getNombre().equals("Cancelado")){
//					pedido.getDetalle().remove(detalle);
//				}
			}
		}
		return pedidosCocina;
	}

	public List<Pedido> listarTodosPedidosActuales(String sucursal) {
		if (!(sucursal.equalsIgnoreCase("Todas"))) return _repository.findByFechaPedidoAndSucursalNombre(obtenerFechaActual(), sucursal);
		else return listarTodosPedidosActuales();
	}

	public int cambiarEstado(Pedido pedido, DetallePedido detalle) {
		boolean terminado = true;
		Integer devolucion = 10;
		List<EstadoPedido> estados = (List<EstadoPedido>) _statusRepository.findAll();
		switch (detalle.getEstado().getNombre()) {
		case "En Espera":
			for (EstadoPedido estado : estados) {
				if (estado.getNombre().equalsIgnoreCase("En Preparaci�n")) {
					detalle.setEstado(estado);
					detalle.setHoraComienzoPreparacion(Calendar.getInstance().getTime());
					_detailRepository.save(detalle);
				}
			}
			break;
		case "En Preparaci�n":
			for (EstadoPedido estado : estados) {
				if (estado.getNombre().equalsIgnoreCase("Preparado")) {
					detalle.setEstado(estado);
					detalle.setHoraFinalPreparacion(Calendar.getInstance().getTime());
					_detailRepository.save(detalle);
				}
			}
			break;
		default:
			devolucion = 0;
		}
		if (pedido.getEstado().getNombre().equalsIgnoreCase("En Espera")) {
			for (EstadoPedido estado : estados) {
				if (estado.getNombre().equalsIgnoreCase("En Preparaci�n")) {
					pedido.setEstado(estado);
					_repository.save(pedido);
					terminado = false;
				}
			}
		} else if (pedido.getEstado().getNombre().equalsIgnoreCase("En Preparaci�n")) {
			for (DetallePedido detalles : pedido.getDetalle()) {
				if (!(detalles.getEstado().getNombre().equalsIgnoreCase("Preparado")) && !(detalles.getEstado().getNombre().equalsIgnoreCase("Cancelado"))) {
					terminado = false;
				}
			}
		}
		if (terminado) {
			for (EstadoPedido estado : estados) {
				if (estado.getNombre().equalsIgnoreCase("Preparado")) {
					pedido.setEstado(estado);
				}
			}
			_repository.save(pedido);
			devolucion = 2;
		} else {
			if (!(devolucion.equals(0))) {
				devolucion = 1;
			}
		}
		return devolucion;
	}

	public void registrarCobro(Pedido pedido) {
		pedido.setEstado(_statusRepository.findByNombreIgnoreCase("Cobrado").get(0));
		_repository.save(pedido);
	}

	public Pedido deshacerAccion(Pedido pedido, DetallePedido detalle, EstadoPedido estado) {
		switch (estado.getNombre()) {
		case "En Espera":
			boolean enEspera = true;
			detalle.setEstado(estado);
			detalle.setHoraComienzoPreparacion(null);
			for (DetallePedido d : pedido.getDetalle()) {
				if ((d.getEstado().getNombre()).equalsIgnoreCase("En Preparaci�n")
						|| (d.getEstado().getNombre()).equalsIgnoreCase("Preparado")) {
					enEspera = false;
				}
			}
			if (enEspera) {
				pedido.setEstado(estado);
			}
			break;

		case "En Preparaci�n":
			detalle.setEstado(estado);
			detalle.setHoraFinalPreparacion(null);
			pedido.setEstado(estado);
			break;

		default:
			break;
		}
		_detailRepository.save(detalle);
		_repository.save(pedido);
		return pedido;
	}

	public Sucursal obtenerSucursal(String sucursal) {
		return _branchService.buscarPorNombre(sucursal);
	}

	public void setSucursalDeUsuario() {
		this.sucursalDelUsuario = _userService.getUsuario().getSucursal().getNombre();
	}

	public List<Pedido> cargarPedidosActuales() {
		return listarTodosPedidosActuales(sucursalDelUsuario);
	}

	public boolean esAdmin() {
		return (_userService.getUsuario().getRol().getNombre()).equalsIgnoreCase("Administrador");
	}

	public void cancelar(Pedido pedido, DetallePedido detalle, String motivoC, String descripcion) {
		boolean cancelable=true;
		for (DetallePedido d : pedido.getDetalle()){
			if(!((d.getEstado()).getNombre().equalsIgnoreCase("Cancelado")) && !(d.equals(detalle))){
				cancelable=false;
			}
		}
		if(cancelable){
			cancelar(pedido, motivoC, descripcion);
		} else{
		
			MotivosCancelacion motivo = obtenerMotivoCancelacion(motivoC);
			EstadoPedido estado = _statusRepository.findByNombreIgnoreCase("Cancelado").get(0);
			detalle.setEstado(estado);
			_detailRepository.save(detalle);
			pedido.setTotal((pedido.getTotal()).subtract(detalle.getSubtotal()));
			_repository.save(pedido);
			Cancelacion cancelacion = new Cancelacion(descripcion, pedido, detalle, motivo);
			_supportService.guardarCancelacion(cancelacion);
		}
	}

	public void modificarDetalle(Pedido pedido, DetallePedido detalle){
		int index = pedido.getDetalle().indexOf(detalle);
		if(index>-1){ //TODO reutilizar el modificar nuevo.
			pedido.getDetalle().get(index).setSubtotal(calcularSubtotal(pedido.getDetalle().get(index).getPrecioProducto(), pedido.getDetalle().get(index).getCantidad()));
			_detailRepository.save(pedido.getDetalle().get(index));
			pedido.setTotal(calcularTotal(pedido.getDetalle()));
			int nroDetalles=-1;
			for (DetallePedido d : pedido.getDetalle()){
				nroDetalles++;
			}
			if(nroDetalles==0){
				pedido.setEstado(buscarEstadoInicial());
			}
			_repository.save(pedido);
			Util.addMessage("Modificaci�n Correcta", "Elemento de Pedido Modificado con �xito.");
		} else {
			Util.addMessageError("Error al Modificar", "Se produjo un error al modificar el elemento de pedido.");
		}
	}

	public Pedido obtenerPedido(Long id) {
		Pedido p = _repository.findOne(id);
		if (p!=null) return quitarGeolocalizacion(p);
		return p;
	}

	public Pedido quitarGeolocalizacion(Pedido p) {
		if(!(p.getCoordenadas()==null)) p.getCoordenadas().setPunto(null);
		p.getSucursal().getGeolocalizacion().setPunto(null);
		return p;
	}

	public Page<Pedido> listarTodosPedidos(Pageable page) {
		Page<Pedido> lista = _repository.findAll(page);
		if (!lista.getContent().isEmpty()) lista.getContent().stream().forEach(p -> quitarGeolocalizacion(p));
		return lista;
	}

	public Respuesta registraPedido(Pedido pedido) {
		Respuesta rta = null;
		try {
			this.validaciones = new Validations();
			pedido = setearFechas(pedido);
			pedido.setEstado(buscarEstadoInicial());
			pedido = verificarRegistracion(pedido);
			pedido = registrarSegunTipoEntrega(pedido);
			if(validaciones.isValid()) {
				savePedido(pedido);
				rta = new Respuesta("201", "Pedido guardado con �xito!", (Object) quitarGeolocalizacion(pedido));
			} else {
	    		rta = new Respuesta("402", validaciones.getMensajes());
	    	}
	
		}catch(Exception e){
			e.printStackTrace();
    		rta = new Respuesta("599", "Error al guardar el Pedido. "+e.toString());
		}
		return rta;
	}
	
	private Pedido registrarSegunTipoEntrega(Pedido pedido) {
		switch (pedido.getTipoEntrega().getNombre()) {
			case "Take Away":
				pedido = confirmarConRetiro(pedido);
				break;
			case "Env�o a Domicilio":
				pedido = confirmarConEnvio(pedido);
				break;
			default:
				/*En caso que se generen nuevos m�todos de env�o, los cuales tengan un ciclo de vida particular, 
				se deber�n agregar m�s casos, o bien, agruparlos dentro de los existentes.*/
				this.validaciones.getMensajes().add(new Mensaje("ERROR","ERROR10: El m�todo de entrega del pedido no corresponde."));
				break;
		}
		return pedido;
	}

	public Respuesta modificarPedido(Long id, Pedido pedido) {
		Respuesta rta = null;
		try {
			this.validaciones = new Validations();
			Pedido pedidoPersistido = obtenerPedido(id);
			verificarModificacion(pedidoPersistido,pedido);
			pedido = modificarSegunTipoEntrega(pedidoPersistido, pedido);
			if(validaciones.isValid()) {
				savePedido(pedido);
				rta = new Respuesta("201", "Pedido guardado con �xito!", (Object) pedido);
			} else {
	    		rta = new Respuesta("403", validaciones.getMensajes());
	    	}
	
		}catch(Exception e){
			e.printStackTrace();
    		rta = new Respuesta("599", "Error al guardar el Pedido. "+e.toString());
		}
		return rta;
	}
	
	private Pedido modificarSegunTipoEntrega(Pedido pedidoPersistido, Pedido pedido) {
		if (!pedido.getTipoEntrega().getId().equals(pedidoPersistido.getTipoEntrega().getId())) { 										 
			if (pedido.getTipoEntrega().getNombre().equalsIgnoreCase("Env�o a Domicilio")) {											 
				PuntoGeografico punto = new PuntoGeografico(pedido.getCoordenadas().getLatitud(),pedido.getCoordenadas().getLatitud());	
				pedido.setCoordenadas(punto);
			}
		}
		pedido.setDetalle(modificarDetalles(pedidoPersistido.getDetalle(), pedido.getDetalle()));
		return pedido;
	}

	private List<DetallePedido> modificarDetalles(List<DetallePedido> detallesPersistidos, List<DetallePedido> detalles) {
		detalles.stream()
					.filter(d -> (d.getId()==null || detallesPersistidos.stream().anyMatch(dp -> !(dp.getId().equals(d.getId())))))
					.forEach(d ->d.setId(crearDetalle(d)));
		return detalles;
	}
	
	public Long crearDetalle(DetallePedido detalle) {
		DetallePedido d = new DetallePedido(detalle.getProducto(), detalle.getSubtotal(), detalle.getCantidad(), detalle.getEstado(), detalle.getNombreProducto(), detalle.getNombreTipoProducto(), detalle.getPrecioProducto());
		return d.getId();
	}
	
	private void verificarModificacion(Pedido pedidoPersistido, Pedido pedido) {
		if((pedidoPersistido==null) || (!pedido.getId().equals(pedidoPersistido.getId()))) this.validaciones.getMensajes()
													.add(new Mensaje("ERROR","ERROR12: No coincide el Pedido a modificar, con el ID informado.")); 
		if (!(pedido.getEstado().getNombre().equalsIgnoreCase("En Espera") || (pedido.getEstado().getNombre().equalsIgnoreCase("En Preparaci�n"))) 
			|| (!(pedidoPersistido.getEstado().getNombre().equalsIgnoreCase("En Espera") || pedidoPersistido.getEstado().getNombre().equalsIgnoreCase("En Preparaci�n")))) {  
			this.validaciones.getMensajes().add(new Mensaje("ERROR","ERROR13: El estado actual del Pedido no permite ser modificado, cont�ctese con la sucursal a cargo de su pedido: "+pedidoPersistido.getSucursal().getTelefono()));
		}
		if (!pedido.getTipoEntrega().getId().equals(pedidoPersistido.getTipoEntrega().getId())) {
			verificarTipoEntregaHabilitada(pedido.getTipoEntrega());
			switch (pedido.getTipoEntrega().getNombre()) {
				case "Env�o a Domicilio":
					verificarDatosEnvio(pedido);
					break;
				default:
					/*En caso que se generen nuevos m�todos de env�o, los cuales tengan un ciclo de vida particular, 
					se deber�n agregar m�s casos, o bien, agruparlos dentro de los existentes.*/
					this.validaciones.getMensajes().add(new Mensaje("ERROR","ERROR10: El m�todo de entrega del pedido no corresponde."));
					break;
			}		 
		}
		verificarSucursalHabilitada(pedido.getSucursal());
		verificarFechaPedido(pedido);
		verificarTipoPagoHabilitado(pedido.getTipoPago());
		verificarDetalles(pedido.getDetalle());
		verificarTotal(pedido);
	}

	private Pedido verificarRegistracion(Pedido pedido) {
		pedido.setSucursal(verificarSucursalHabilitada(pedido.getSucursal()));
		verificarTipoEntregaHabilitada(pedido.getTipoEntrega());
		verificarTipoPagoHabilitado(pedido.getTipoPago());
		verificarDetalles(pedido.getDetalle());
		verificarTotal(pedido);
		return pedido;
	}

	public Sucursal verificarSucursalHabilitada(Sucursal sucursal) {
		Sucursal suc = _branchService.obtenerSucursal(sucursal.getId());
		if (!suc.getHabilitado()) {
			this.validaciones.getMensajes().add(new Mensaje("ERROR","ERROR3: La sucursal seleccionada no se encuentra habilitada para realizar su pedido.")); 
			return null;
		} else {
			return suc;
		}
	}

	public void verificarTipoEntregaHabilitada(TipoEntrega tipo) {
		if (!tipo.isHabilitado()) this.validaciones.getMensajes()
									.add(new Mensaje("ERROR","ERROR4: El Tipo de Entrega seleccionado no se encuentra habilitado.")); 
	}

	public void verificarTipoPagoHabilitado(TipoPago tipo) {
		if (!tipo.isHabilitado()) this.validaciones.getMensajes()
									.add(new Mensaje("ERROR","ERROR5: El Tipo de Pago seleccionado no se encuentra habilitado.")); 
	}

	public void verificarTotal(Pedido pedido) {
		BigDecimal total = calcularTotal(pedido.getDetalle());
		if(!total.equals(pedido.getTotal())) this.validaciones.getMensajes()
												.add(new Mensaje("ERROR","ERROR9: El valor correspondiente al total del pedido no corresponde."));
	}

	public void verificarProductos(List<DetallePedido> detalles) {
		List<Producto> productos = new ArrayList<>();
		detalles.stream()
					.forEach(detalle -> productos.add(detalle.getProducto()));
		if(_productService.validarProductos(productos)) this.validaciones.getMensajes()
												.add(new Mensaje("ERROR","ERROR8: Se encontraron productos no existentes."));
	}
	
	public Pedido confirmarConEnvio(Pedido pedido) {
		verificarDatosEnvio(pedido);
		PuntoGeografico punto = new PuntoGeografico(pedido.getCoordenadas().getLatitud(),pedido.getCoordenadas().getLatitud());
		pedido.setSucursal(obtenerSucursalParaEnvio(punto)); 
		Pedido pedidoAPersistir = new Pedido(pedido.getFechaPedido(), pedido.getHoraPedido(), pedido.getFechaEntrega(), pedido.getHoraEntrega(), pedido.getDetalle(), 
										pedido.getTotal(), pedido.getEstado(), pedido.getSucursal(), pedido.getTipoPago(), pedido.getTipoEntrega(), 
										punto, pedido.getCalle(), pedido.getNumero(), pedido.getDpto(), pedido.getPiso(), pedido.getComentarios());
		return pedidoAPersistir;
	}

	private void verificarDatosEnvio(Pedido pedido) {
		if (pedido.getCalle().isEmpty() || pedido.getNumero()<=0 || pedido.getCoordenadas() == null) this.validaciones.getMensajes()
														.add(new Mensaje("ERROR","ERROR11: No se han proporcionados datos suficientes como para enviarle su pedido."));
	}

	public Pedido setearFechas(Pedido pedido) {
		pedido.setFechaPedido(obtenerFechaActual());
		pedido.setHoraPedido(obtenerFechaActual());
		verificarFechaPedido(pedido);
		if(pedido.getFechaEntrega() == null) {
			pedido.setFechaEntrega(obtenerFechaEntrega());
			pedido.setHoraEntrega(obtenerFechaEntrega());
		} else {
			verificarFechaPedido(pedido);
		}
		return pedido;
	}

	public Sucursal obtenerSucursalParaEnvio(PuntoGeografico pto) {
		return _branchService.obtenerSucursalParaEnvio(pto);
	}

	public Pedido confirmarConRetiro(Pedido pedido) {
		Pedido pedidoAPersistir = new Pedido(pedido.getFechaPedido(), pedido.getHoraPedido(), pedido.getFechaEntrega(), pedido.getHoraEntrega(), 
												pedido.getDetalle(), pedido.getTotal(), pedido.getEstado(), pedido.getSucursal(), pedido.getTipoPago(), 
												pedido.getTipoEntrega(), pedido.getComentarios());
		return pedidoAPersistir;
	}

	private Pedido savePedido(Pedido pedido) {
		saveDetallesPedidos(pedido.getDetalle());
		return _repository.save(pedido);
	}

	public List<DetallePedido> saveDetallesPedidos(List<DetallePedido> detalles) {
		return (List<DetallePedido>) _detailRepository.save(detalles);
	}
	
	public DetallePedido saveDetallePedido(DetallePedido detalle) {
		return _detailRepository.save(detalle);
	}
	
	public void verificarDetalles(List<DetallePedido> detalles) {
		if(detalles.isEmpty()) this.validaciones.getMensajes().add(new Mensaje("ERROR","ERROR6: No hay �tems en el pedido solicitado."));
		if(!detalles.stream()
						.filter(d -> !d.getSubtotal().equals(this.calcularSubtotal(d.getPrecioProducto(), d.getCantidad())))
						.collect(Collectors.toList()).isEmpty()) {
			this.validaciones.getMensajes().add(new Mensaje("ERROR","ERROR7: En al menos un �tem del pedido, el subtotal no coincide con la relaci�n Precio * Cantidad."));
		}
		this.verificarProductos(detalle);
	}

	public void verificarFechaPedido(Pedido pedido) {
		/* TODO 
		 * Validar que la fecha-hora de entrega propuesta por el cliente est� dentro del horario laboral establecido por la sucursal.
		 * En caso OK: No se realiza ninguna acci�n.
		 * En caso contrario, se debe cargar un Mensaje con c�digo de respuesta 402:RegistryError, explicando la causa.
		 * */
	}
	
	public void setSucursalesHabilitadas(List<Sucursal> sucursalesHabilitadas) {
		this.sucursalesHabilitadas = sucursalesHabilitadas;
	}

	public BigDecimal getTotal() {
		return this.calcularTotal(detalle);
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public List<DetallePedido> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<DetallePedido> detalle) {
		this.detalle = detalle;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}
	
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public List<TipoPago> obtenerTipoPagoHabilitados() {
		return _supportService.listarTodosHabilitadosTipoPago();
	}

	public List<TipoEntrega> obtenerTipoEntregaHabilitadas() {
		return _supportService.listarTodosHabilitadosTipoEntrega();
	}
	
//	public void modificarDetalle(Pedido pedido, DetallePedido detalle, Producto prod, Integer cantidad) {  METODO para cuendo se quiere modificar un Detalla de pedido
//		int index = pedido.getDetalle().indexOf(detalle);												  Y se modifica el producto del detalle, lo que conlleva otras
//		boolean noExiste = true;																		  verificaciones extras, como que ese producto no exista en
//		if (index>-1){																					  en otro detalle dentro del mismo pedido.
//			for (DetallePedido d : pedido.getDetalle()){
//				if((d.getProducto().equals(prod))){
//					int pos = pedido.getDetalle().indexOf(d);
//					noExiste = false;
//					pedido.getDetalle().get(pos).setCantidad(cantidad+(pedido.getDetalle().get(pos).getCantidad()));
//					pedido.getDetalle().get(pos).setSubtotal(calcularSubtotal(prod, cantidad));
//					repositorioDetallePedido.save(pedido.getDetalle().get(pos));
//					pedido.getDetalle().remove(detalle);
//					pedido.setTotal(calcularTotal(pedido.getDetalle()));
//					repositorioDetallePedido.save(pedido.getDetalle());
//					repositorioPedido.save(pedido);
//					repositorioDetallePedido.delete(pedido.getDetalle().get(index));
//					
//				}
//			}
//			if (noExiste){
//				pedido.getDetalle().get(index).setProducto(prod);
//				pedido.getDetalle().get(index).setCantidad(cantidad);
//				pedido.getDetalle().get(index).setSubtotal(calcularSubtotal(prod, cantidad));
//				pedido.getDetalle().get(index).setNombreProducto(prod.getNombre());
//				pedido.getDetalle().get(index).setNombreTipoProducto(prod.getTipoProducto().getNombre());
//				pedido.getDetalle().get(index).setPrecioProducto(prod.getPrecio());
//				pedido.getDetalle().get(index).setEstado(estadoInicial);
//				repositorioDetallePedido.save(pedido.getDetalle().get(index));
//				pedido.setTotal(calcularTotal(pedido.getDetalle()));
//				repositorioPedido.save(pedido);
//			}
//			pedido.setTotal(calcularTotal(pedido.getDetalle()));
//			repositorioPedido.save(pedido);
//			Util.addMessage("Modificaci�n Correcta", "Elemento de Pedido Modificado con �xito.");
//		} else{
//			Util.addMessageError("Error al Modificar", "Se produjo un error al modificar el elemento de pedido.");
//		}
//	}

}