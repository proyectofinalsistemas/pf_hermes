package hermes.dominio;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity
public class CronogramaSucursal {

	@Id @GeneratedValue
	private Integer id;
	private @NonNull @NotNull String dia;
	private @NonNull @NotNull Sucursal sucursal;
	private boolean trabaja;
	@Temporal(TemporalType.TIME)
	private Date horaComienzoMañana;
	@Temporal(TemporalType.TIME)
	private Date horaFinMañana;
	@Temporal(TemporalType.TIME)
	private Date horaComienzoTarde;
	@Temporal(TemporalType.TIME)
	private Date horaFinTarde;
}
