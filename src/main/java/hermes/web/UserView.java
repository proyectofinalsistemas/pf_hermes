package hermes.web;

import hermes.dominio.Rol;
import hermes.dominio.Sucursal;
import hermes.dominio.Usuario;
import hermes.logica.UserService;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.swing.event.ChangeEvent;

import org.apache.jasper.tagplugins.jstl.core.Set;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

import Utilidades.Util;

@RestController
@Scope("view")
public class UserView implements Serializable {

	@Autowired
	private UserService servicio;
	
	private List<Usuario> usuarios;
	private Usuario usuarioSeleccionado;
	private List<Rol> roles;
	private String rolSeleccionado;
	private List<Sucursal> sucursales;
	private String sucursalSeleccionada;
	private String nombreUsuario;
	private String password;
	private String password2;
	private String repetirPassword;
	private @Setter @Getter String mail;
	private Rol rol;
	private Sucursal sucursal;
	private Sucursal sucursalAdmin;
	private @Getter	@Setter List<Usuario> filtroUsuarios; 
	private boolean estado;
	private boolean noHayUsuarioSeleccionado;
	private @Setter @Getter boolean esAdmin;
	private @Setter @Getter String lblSucursal;
	private String parametro;

	@PostConstruct
	public void init() {
		this.cargarUsuarios();
		noHayUsuarioSeleccionado = true;
		esAdmin = false;
		this.cargarRoles();
		this.cargarSucursales();
	}

	public void cargarUsuarios() {
		this.usuarios = servicio.listarTodosUsuarios();
	}

	public void cargarRoles() {
		roles = servicio.listarTodosRoles();
	}
	
	public void cargarSucursales() {
		int index = 0;
		sucursales = servicio.listarTodasSucursales();
		setLblSucursal("Seleccione una Sucursal");
		for(Sucursal suc : sucursales){
			if (suc.getNombre().equalsIgnoreCase("Todas")){
				setSucursalAdmin(suc);
				index = sucursales.indexOf(suc);
			}
		}
		sucursales.remove(index);
	}

	public void validarUsuarioSeleccionado() {
		if (usuarioSeleccionado == null) {
			noHayUsuarioSeleccionado = true;
		} else {
			noHayUsuarioSeleccionado = false;
		}
	}

	public void save() {
		if (validarPasswordsIguales() & !validarUsuarioRepetido()) {
			List<Rol> roles = new ArrayList<>();
			try{
				roles.add(servicio.obtenerRol(rolSeleccionado));
			}catch(Exception e){}
			if(rolSeleccionado.equals("Administrador")){
				setSucursal(sucursalAdmin);
			} else {
				setSucursal(servicio.obtenerSucursal(sucursalSeleccionada));
			}
			try{
			servicio.guardarUsuario(nombreUsuario, password, roles, sucursal, mail, estado);
			addMessage("Guardado con �xito", "Datos guardados");
			} catch(Exception e){
				addMessage("Error al guardar"+e, "");
			}
			this.cargarUsuarios();
			this.borrarVista();
			deshabilitarBotones();
			this.cerrarDialog("dlgNuevo");
		}
	}

	public boolean validarPasswordsIguales() {
		boolean passwordsIguales;
		passwordsIguales = servicio.passwordsIguales(password, password2);
		if (!passwordsIguales) {
			addMessage("Las contraseñas ingresadas no coinciden", "");
		}
		return passwordsIguales;
	}

	public boolean validarUsuarioRepetido() {
		boolean usuarioRepetido;
		usuarioRepetido = servicio.usuarioRepetido(nombreUsuario);
		if (usuarioRepetido) {
			addMessage("Usuario ya existente", "");
		}
		return usuarioRepetido;
	}
	public boolean validarUsuarioRepetido(Usuario usuario) {
		boolean usuarioRepetido;
		if((((usuarioSeleccionado.getUsuario()).toLowerCase()).equals(nombreUsuario.toLowerCase()))&&((usuarioSeleccionado.getRol().getNombre()).equals(rolSeleccionado))&&
				((usuarioSeleccionado.getSucursal().getNombre()).equals(sucursalSeleccionada))&&((usuarioSeleccionado.getMail()).equals(mail)) &&((usuarioSeleccionado.getHabilitado())==estado)){
			addMessage("No Guardado", "No modificó ningún dato.");
			usuarioRepetido=true;
		} else{
			usuarioRepetido = servicio.usuarioRepetido(usuario, nombreUsuario);
			if (usuarioRepetido) {
				addMessage("Usuario ya existente", "");
			}
		}
		return usuarioRepetido;
	}
	
	public void update() {
		if (!validarUsuarioRepetido(usuarioSeleccionado)) {
			List<Rol> roles = new ArrayList<>();
			try{
				roles.add(servicio.obtenerRol(rolSeleccionado));
			}catch(Exception e){}
			if(rolSeleccionado.equals("Administrador")){
				setSucursal(sucursalAdmin);
			} else {
				sucursal = servicio.obtenerSucursal(sucursalSeleccionada);
			}
			servicio.updateUsuario(usuarioSeleccionado, nombreUsuario, roles, sucursal, mail, estado);
			this.borrarVista();
			deshabilitarBotones();
			this.cerrarDialog("dlgModificar");
			addMessage("Actualizado con éxito", "Datos actualizados");
		}
	}
	
	public void cambiarEstado() {
		servicio.cambiarEstado(usuarioSeleccionado);
		addMessage("Estado cambiado con éxito", "Cambio de estado realizado con éxito.");
		deshabilitarBotones();
	}

	public void cerrarDialog(String dialog) {
		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("PF('" + dialog + "').hide()");
	}

	public void eventoDobleClick() {
		modificarUsuario();
		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("PF('dlgModificar').show()");
	}

	public void addMessage(String summary, String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Usuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
		if (usuarioSeleccionado != null) {		
			usuarioSeleccionado.getRol().getNombre();
			this.setSucursalSeleccionada(usuarioSeleccionado.getSucursal().getNombre());
			this.setEstado(usuarioSeleccionado.getHabilitado());
		}
	}

	public List<Rol> getRoles() {
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}

	public String getRolSeleccionado() {
		return rolSeleccionado;
	}

	public void setRolSeleccionado(String rolSeleccionado) {
		this.rolSeleccionado = rolSeleccionado;
	}
	
	public void esAdmin(){
		if(rolSeleccionado.equals("Administrador")){
			esAdmin=true;
			setLblSucursal(sucursalAdmin.getNombre());
			setSucursalSeleccionada(lblSucursal);
		} else {
		esAdmin = false;
		setLblSucursal("Seleccione una Sucursal");
		}
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public List<Sucursal> getSucursales() {
		return sucursales;
	}

	public void setSucursales(List<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public String getSucursalSeleccionada() {
		return sucursalSeleccionada;
	}

	public void setSucursalSeleccionada(String sucursalSeleccionada) {
		this.borrarSucursal();
		this.sucursalSeleccionada = sucursalSeleccionada;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.borrarPassword();
		this.password = password;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public boolean isNoHayUsuarioSeleccionado() {
		return noHayUsuarioSeleccionado;
	}

	public void setNoHayUsuarioSeleccionado(boolean noHayUsuarioSeleccionado) {
		this.noHayUsuarioSeleccionado = noHayUsuarioSeleccionado;
	}

	public void onRowSelect(SelectEvent event) {
		this.validarUsuarioSeleccionado();
	}

	public void onRowUnselect(UnselectEvent event) {
		this.validarUsuarioSeleccionado();
	}

	public void deshabilitarBotones() {
		this.usuarioSeleccionado = null;
		setNoHayUsuarioSeleccionado(true);
	}

	public void borrarVista() {
		rolSeleccionado = null;
		sucursalSeleccionada = "";
		nombreUsuario = "";
		password = "";
		password2 = "";
		this.mail="";
		estado = true;
		esAdmin = false;
		setLblSucursal("Seleccione una Sucursal");
	}

	public void borrarPassword() {
		password = "";
	}

	public void modificarUsuario() {
		esAdmin=false;
		setNombreUsuario(usuarioSeleccionado.getUsuario());
		rolSeleccionado = usuarioSeleccionado.getRol().getNombre();
		if((rolSeleccionado).equals("Administrador")){
			esAdmin=true;
			setLblSucursal(sucursalAdmin.getNombre());
			setSucursalSeleccionada(lblSucursal);
		}
		setMail(usuarioSeleccionado.getMail());
		setEstado(usuarioSeleccionado.getHabilitado());
	}
	
    public void reestablecerPassword(){
    	if (!(this.parametro.equals(""))){
    		servicio.reestablecerPassword(this.parametro);
    		
    	} else {
    		Util.addMessage("No ingresó ningún dato.", "Error");
    	}
    }


	public void borrarRol() {
		rolSeleccionado = null;
		rol = null;
	}

	public void borrarSucursal() {
		sucursalSeleccionada = "";
		sucursal = null;
	}

	public boolean getEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getRepetirPassword() {
		return repetirPassword;
	}

	public void setRepetirPassword(String repetirPassword) {
		this.repetirPassword = repetirPassword;
	}

	public Sucursal getSucursalAdmin() {
		return sucursalAdmin;
	}

	public void setSucursalAdmin(Sucursal sucursalAdmin) {
		this.sucursalAdmin = sucursalAdmin;
	}
	public String getParametro() {
		return parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}
}
