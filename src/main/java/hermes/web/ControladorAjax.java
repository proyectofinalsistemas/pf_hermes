package hermes.web;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SessionScoped
public class ControladorAjax implements Serializable {

	 private String page;
	 private String nombre;
	 private static final long serialVersionUID = 1L;
	 
	 @PostConstruct
     public void init() {
         page = "vacio"; //  Default include.
     }

     public String getPage() {
         return page;
     }
  
     public void setPage(String page) {
         this.page = page;
         RequestContext.getCurrentInstance().addCallbackParam("page", page + ".xhtml");
     }

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
