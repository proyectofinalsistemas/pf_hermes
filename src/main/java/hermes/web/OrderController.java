package hermes.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import Utilidades.Respuesta;
import hermes.dominio.Cancelacion;
import hermes.dominio.Pedido;
import hermes.logica.OrderService;

@RestController
@RequestMapping("/pedido")
@CrossOrigin(origins = "*")
public class OrderController {

	@Autowired
	private OrderService _service;
	
	// CALL CONSULTAS
	@GetMapping
	public Page<Pedido> getAllPedidos(Pageable page) {
		return _service.listarTodosPedidos(page);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Pedido getOnePedido(@PathVariable(name = "id") Long id) {
		return _service.obtenerPedido(id);
	}
		
	// ALTA
	@PostMapping
	public Respuesta registrarOrdenDeServicio(@RequestBody Pedido pedido) {
		return _service.registraPedido(pedido);
	}
	
	// MODIFICACIÓN
    @RequestMapping(value = "/{id}" , method = RequestMethod.PUT)
    public Respuesta modificaPedido(@RequestBody Pedido pedido, @PathVariable(name = "id") Long id) {
    	return _service.modificarPedido(id, pedido);
    }
	
	// CAMBIO ESTADOS
    @RequestMapping(value = "/cancelar" , method = RequestMethod.PUT)
    public Respuesta cancelarPedido(@RequestBody Cancelacion cancelacion) {
    	return _service.cancelar(cancelacion);
    }
    
    @RequestMapping(value = "/detalle/cancelar" , method = RequestMethod.PUT)
    public Respuesta cancelarDetalle(@RequestBody Cancelacion cancelacion) {
    	return _service.cancelar(cancelacion);
    }
}
