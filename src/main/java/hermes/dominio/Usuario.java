package hermes.dominio;

import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity 
public class Usuario implements UserDetails{
	@Id @GeneratedValue
	private Integer id;
	protected boolean habilitado;
	@NotNull
	private @NonNull String usuario;
	@NotNull
	private @NonNull String password;
	@ManyToMany(fetch = FetchType.EAGER)
	@NonNull @NotNull
	private List<Rol> rol;
	@ManyToOne @NonNull @NotNull
	private Sucursal sucursal;
	@NotNull
	private @NonNull String mail;
	private boolean reestablecerPass; 

	public Usuario(String usuario, String password, List<Rol> roles, Sucursal sucursal, String mail, boolean estado){
		this.setUsuario(usuario);
		this.setPassword(password);
		this.setRol(roles);
		this.setSucursal(sucursal);
		this.setMail(mail);
		this.setHabilitado(estado);
		this.setReestablecerPass(false);
	}
	
	public Rol getRol(){
		return rol.get(0);
	}
	public boolean getHabilitado(){
		return this.habilitado;
	}
	public Integer getId() {
		return id;
	}
	
	public void setHabilitado(boolean bool){
		this.habilitado= bool;
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return rol;
	}
	@Override
	public String getPassword(){
		return password;
	}
	@Override
	public String getUsername() {
		return usuario;
	}
	
	public boolean getReestablecerPassword(){
		return this.reestablecerPass;
	}
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	@Override
	public boolean isEnabled() {
		return habilitado;
	}

}
