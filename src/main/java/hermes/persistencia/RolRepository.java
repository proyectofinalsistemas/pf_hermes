package hermes.persistencia;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.Rol;

@Repository
public interface RolRepository extends PagingAndSortingRepository<Rol, Integer> {

	Rol findByNombre(String nombreRol);	
}
