package hermes.dominio;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity 
public class DetallePedido {
	
	@Id @GeneratedValue @NotNull
	private Long id;
	@OneToOne @NotNull
	private @NonNull Producto producto;
	@Min(0) @NotNull
	private @NonNull BigDecimal subtotal;
	@Min(1) @NotNull
	private @NonNull Integer cantidad;
	@ManyToOne
	private @NonNull @NotNull EstadoPedido estado;	
	private String comentarios;
	@Temporal(TemporalType.TIME)
	private Date horaComienzoPreparacion;
	@Temporal(TemporalType.TIME)
	private Date horaFinalPreparacion;
	
	//Replicado de campos para lograr mayor consistencia.
	private @NonNull @NotNull String nombreProducto;
	private @NonNull @NotNull String nombreTipoProducto;
	private @NonNull @NotNull BigDecimal precioProducto;

}
