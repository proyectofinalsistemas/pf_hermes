package Utilidades;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Respuesta {

	String estado;
	List<Mensaje> mensajes;
	Object objeto;
	
	//Para confirmación exitosa
	public Respuesta(String estado, String mensaje, Object obj) {
		setEstado(estado);
		ArrayList<Mensaje> array = new ArrayList<Mensaje>();
		array.add(new Mensaje(estado , mensaje));
		setMensajes(array);
		setObjeto(obj);
	}
	
	//Para lista de errores
	public Respuesta(String estado, List mensajes, Object obj) {
		setEstado(estado);
		setMensajes(mensajes);
		setObjeto(obj);
	}

	//Para confirmación exitosa sin objeto
	public Respuesta(String estado, String mensaje) {
		setEstado(estado);
		ArrayList<Mensaje> array = new ArrayList<Mensaje>();
		array.add(new Mensaje(estado , mensaje));
		setMensajes(array);
	}
	
	//Para lista de errores sin objeto
	public Respuesta(String estado, List mensajes) {
		setEstado(estado);
		setMensajes(mensajes);
	}
}
