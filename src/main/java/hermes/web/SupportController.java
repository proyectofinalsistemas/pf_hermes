package hermes.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hermes.dominio.TipoEntrega;
import hermes.dominio.TipoPago;
import hermes.logica.SupportService;

@RestController
@RequestMapping("/soporte")
@CrossOrigin(origins = "*")
public class SupportController {
	@Autowired
	private SupportService _servicio;
	
	//CALL CONSULTAS 
	//TipoEnvio
	@RequestMapping(value = "/tipoentrega", method = RequestMethod.GET)
	public List<TipoEntrega> getAllHabilitadosTipoEntrega(){
		return _servicio.listarTodosHabilitadosTipoEntrega(); 
	}
	
	@RequestMapping(value = "/tipoentrega/{id}", method = RequestMethod.GET)
	public Object getOneTipoEnvio(@PathVariable(name = "id") Integer id){
		if(!id.equals(0))	return _servicio.obtenerTipoEntrega(id);
		else return _servicio.listarTodosTipoEntrega();
	}
	
	//TipoPago
	@RequestMapping(value = "/tipopago", method = RequestMethod.GET)
	public List<TipoPago> getAllHabilitadosTipoPago(){
		return _servicio.listarTodosHabilitadosTipoPago(); 
	}
	
	@RequestMapping(value = "/tipopago/{id}", method = RequestMethod.GET)
	public Object getOneTipoPago(@PathVariable(name = "id") Integer id){
		if(!id.equals(0))	return _servicio.obtenerTipoPago(id);
		else return _servicio.listarTodosTipoPago();
	}
}
