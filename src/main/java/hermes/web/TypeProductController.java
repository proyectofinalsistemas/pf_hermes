package hermes.web;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hermes.dominio.TipoProducto;
import hermes.logica.ProductService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/tipoproducto")
public class TypeProductController {
	
	@Autowired
	private ProductService _servicio;
	
	// CALL CONSULTAS
	@GetMapping
	public List<TipoProducto> getAllTipoPrductos(){
		return _servicio.listarTodosTipoProductoHabilitados();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public TipoProducto getTipoPrducto(@PathVariable(name = "id") Integer id){
		try {
			System.out.println(id);
			return _servicio.obtenerTipoProducto(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
