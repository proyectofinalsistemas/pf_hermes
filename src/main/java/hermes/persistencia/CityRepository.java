package hermes.persistencia;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.Ciudad;

@Repository
public interface CityRepository extends PagingAndSortingRepository<Ciudad, Integer>{
	
	public List<Ciudad> findByProvinciaNombre(String nombre);
	
}
