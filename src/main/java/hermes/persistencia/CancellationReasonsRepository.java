package hermes.persistencia;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.MotivosCancelacion;

@Repository
public interface CancellationReasonsRepository extends PagingAndSortingRepository<MotivosCancelacion, Integer> {
	public List<MotivosCancelacion> findByDescripcion(String descripcion);

}
