package hermes.persistencia;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.TipoPago;

@Repository
public interface PaymentTypeRepository extends PagingAndSortingRepository<TipoPago,Integer>{

	List<TipoPago> findByHabilitado(boolean b);
	
}
