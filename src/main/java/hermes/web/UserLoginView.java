package hermes.web;

import hermes.dominio.Usuario;
import hermes.logica.UserService;
import lombok.Getter;
import lombok.Setter;

import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import java.util.List;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;

import Utilidades.Util;

@RestController
@SessionScoped
public class UserLoginView {
	@Autowired
	private UserService servicio;
	private String username;
	private String password;
	private @Setter @Getter String nombreUsuario;
	private @Setter @Getter String nvaPassword;
	private @Setter @Getter String validarPassword;

	private List<Usuario> listaUsuarios;
	boolean loggedIn = false;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isRolOfUserLogged(String rol) {
		boolean isRol = false;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		for (GrantedAuthority authority : authentication.getAuthorities()) {
			if (authority.getAuthority().equals(rol))
				isRol = true;
		}
		return isRol;
	}

	public boolean isRolsOfUserLogged(String rol1, String rol2) {
		boolean isRol = false;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		for (GrantedAuthority authority : authentication.getAuthorities()) {
			if (authority.getAuthority().equals(rol1) || authority.getAuthority().equals(rol2))
				isRol = true;
		}
		return isRol;
	}

	public boolean isRolOfUserLogged(String rol1, String rol2) {
		boolean isRol = false;
		boolean isRol1 = false;
		boolean isRol2 = false;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		for (GrantedAuthority authority : authentication.getAuthorities()) {
			if (authority.getAuthority().equals(rol1))
				isRol1 = true;
			if (authority.getAuthority().equals(rol2))
				isRol2 = true;
		}
		isRol = isRol1 && !isRol2;
		return isRol;
	}

	public String getUserLogged() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		reestablecioPassword(authentication.getName());
		return authentication.getName();
	}

	public void login(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage message = null;
		loggedIn = false;
		listaUsuarios = servicio.buscarPorNombreYPassword(username, password);
		if (listaUsuarios.size() > 0) {
			loggedIn = true;
			message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", username);
		} else {
			loggedIn = false;
			message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Credenciales Incorrectas");
		}
		FacesContext.getCurrentInstance().addMessage(null, message);
		context.addCallbackParam("loggedIn", loggedIn);
		if (loggedIn = true)
			context.addCallbackParam("view", "principal.xhtml");
	}
	
	public void reestablecioPassword(String user){
		boolean bool = servicio.reestablecioPassword(user);
    	if(bool){
    		Usuario usuario = servicio.getUsuario();
    		setNombreUsuario(usuario.getUsuario());
    		RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('dlgReestablecioPassword').show()");	
    	}
    }
	
	public void guardar(){
		boolean validar = validarPassword();
		if(validar){
			servicio.guardarUsuarioReestablecido(getNombreUsuario(), getNvaPassword());
		}
		cerrarDlg("dlgReestablecioPassword");
		limpiarVariables();
	}
	
	public void limpiarVariables() {
		setNombreUsuario("");
		setNvaPassword("");
		setValidarPassword("");
	}

	public void cerrarDlg(String dlg) {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('"+ dlg +"').hide()");
	}

	public boolean validarPassword(){
		return (getNvaPassword().equals(getValidarPassword()));
	}
}
