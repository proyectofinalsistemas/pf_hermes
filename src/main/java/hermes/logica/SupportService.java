package hermes.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import hermes.dominio.Cancelacion;
import hermes.dominio.MotivosCancelacion;
import hermes.dominio.TipoEntrega;
import hermes.dominio.TipoPago;
import hermes.persistencia.CancellationReasonsRepository;
import hermes.persistencia.CancellationRepository;
import hermes.persistencia.DelliveryTypeRepository;
import hermes.persistencia.PaymentTypeRepository;

@Service
public class SupportService {
	@Autowired
	private CancellationReasonsRepository _cancellationReasonsRepository;
	@Autowired
	private CancellationRepository _cancelationRepository;
	@Autowired
	private DelliveryTypeRepository _delliveryRepository;
	@Autowired
	private PaymentTypeRepository _paymentRepository;
	
	//ALTA
	public void guardarCancelacion(Cancelacion cancelacion) {
		_cancelationRepository.save(cancelacion);	
	}

	//CONSULTAS
	public List<MotivosCancelacion> listarTodosMotivosCancelacion() {
		return (List<MotivosCancelacion>) _cancellationReasonsRepository.findAll();
	}

	public TipoEntrega obtenerTipoEntrega(Integer id) {
		return _delliveryRepository.findOne(id);
	}

	public List<TipoEntrega> listarTodosHabilitadosTipoEntrega() {
		return _delliveryRepository.findByHabilitado(true);
	}
	
	public List<TipoEntrega> listarTodosTipoEntrega() {
		return (List<TipoEntrega>) _delliveryRepository.findAll();
	}
	
	public TipoPago obtenerTipoPago(Integer id) {
		return _paymentRepository.findOne(id);
	}

	public List<TipoPago> listarTodosHabilitadosTipoPago() {
		return _paymentRepository.findByHabilitado(true);
	}

	public Object listarTodosTipoPago() {
		return (List<TipoPago>) _paymentRepository.findAll();
	}
}
