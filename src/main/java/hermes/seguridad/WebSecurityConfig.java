package hermes.seguridad;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private CustomSuccessHandler customSuccessHandler;	

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.csrf().disable();
    	http.authorizeRequests().
    		antMatchers("/index").permitAll().
			antMatchers("/secure/atencionCliente/**").access("hasAuthority('Encargado de Atenci�n al Cliente') or hasAuthority('Administrador')").
			antMatchers("/secure/cocina/**").access("hasAuthority('Cocinero') or hasAuthority('Administrador')").
			antMatchers("/secure/**").access("hasAuthority('Administrador')").
		and().
		formLogin().  //login configuration
        	loginPage("/index.xhtml").
            loginProcessingUrl("/appLogin").
            usernameParameter("app_username").
            passwordParameter("app_password").
            successHandler(customSuccessHandler).
		and(). 		 //Antes hab�a un . en lugar de un ;
		logout().    //logout configuration
			logoutUrl("/appLogout"). 
			logoutSuccessUrl("/index.xhtml");		
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
            auth.userDetailsService(userDetailsService);
    }
}
