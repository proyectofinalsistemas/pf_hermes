package hermes.logica;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Set;

import hermes.dominio.Producto;
import hermes.dominio.Rol;
import hermes.dominio.Sucursal;
import hermes.dominio.Usuario;
import hermes.persistencia.RolRepository;
import hermes.persistencia.BranchRepository;
import hermes.persistencia.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Utilidades.Util;

@Service
public class UserService {

	@Autowired
	private BranchService _servicioSucursal;
	@Autowired
	private UserRepository _repositorio;
	@Autowired
	private RolRepository _repositorioRol;

	private List<Sucursal> sucursales;
	private Usuario usuario;
	
	public void guardarUsuario(String nombre, String password, List<Rol> roles, Sucursal sucursal, String mail, boolean estado) {
		Usuario usuario = new Usuario(nombre, password, roles, sucursal, mail, estado);
		guardarUsuario(usuario);
	}

	public List<Usuario> listarTodosUsuarios(){
		return (List<Usuario>) _repositorio.findAll();
	}
	
	public List<Rol> listarTodosRoles(){
		return (List<Rol>)_repositorioRol.findAll();		
	}
	
	public List<Sucursal> listarTodasSucursales() {
		return _servicioSucursal.listarTodasSucursales();
	}
	
	public List<Usuario> buscarPorNombre(String nombre){
		return _repositorio.findByUsuario(nombre);
	}
	
	public List<Usuario> buscarPorNombreYPassword(String nombre,String pass ){
		String password= Util.sha512(pass);
		return _repositorio.findByUsuarioAndPasswordAndHabilitado(nombre,password, true);
	}
	
	
	public Rol obtenerRol(String nombreRol){
		return _repositorioRol.findByNombre(nombreRol);
	}
	
	public Sucursal obtenerSucursal(String nombreSucursal) {
		return _servicioSucursal.buscarPorNombre(nombreSucursal);
	}
	
	public boolean passwordsIguales(String password, String passwordRepetido){
		return password.equals(passwordRepetido);
	}
	
	public boolean usuarioRepetido(String u){
		if(listarTodosUsuarios().stream()
								.anyMatch(us -> us.getUsuario().equalsIgnoreCase(Util.removeAcentos(u)))) return true;
		else return false;								
	}
	
	public void updateUsuario(Usuario usuario, String nombreUsuario, List<Rol> rol, Sucursal sucursal, String mail, boolean estado){	
		usuario.setUsuario(nombreUsuario);
		usuario.setRol(rol);
		usuario.setSucursal(sucursal);
		usuario.setHabilitado(estado);
		usuario.setMail(mail);
		guardarUsuario(usuario);
	}
	
	public void cambiarEstado(Usuario usuario){
		usuario.setHabilitado(!usuario.getHabilitado());
		guardarUsuario(usuario);
	}

	public boolean usuarioRepetido(Usuario usuarioSeleccionado, String nombreUsuario) {
		List<Usuario> users = _repositorio.findByUsuario(Util.removeAcentos(nombreUsuario));
		if((users.isEmpty()) || users.stream()
								.allMatch(us -> (us.getId().equals(usuarioSeleccionado.getId())))) return false;
		else return true;
	}

	public void reestablecerPassword(String parametro) {
		Usuario user=null;
		if(parametro.contains("@")){
			try{
				user = (_repositorio.findByMail(parametro)).get(0);
			} catch (Exception e){
				Util.addMessage("Error", "Error al reestablecer cuenta a trav�s del e-mail.");
			}
		} else {
			try{
				user = (_repositorio.findByUsuario(parametro)).get(0);
			} catch (Exception e){
				Util.addMessage("Error", "Error al reestablecer cuenta a trav�s del usuario.");
			}
		}
		try{
			if(!user.equals(null)){
				String passwordNueva = Util.generarPassword();
				String asunto = "Sistema Hermes - Reestablecer Cuenta";
				//TODO cambiar a StringBuilder.
				String mensaje = "Hola "+ user.getUsuario()+", nos comunicamos contigo por una solicitud de reestablecimiento de cuenta."+
						" A continuaci�n aparece un c�digo, el cual utilizar� para loguearse: Usuario: "+user.getUsuario()+". Contrase�a: "+ 
						passwordNueva+" . Dicha contrase�a le servir� por una �nica vez, dado que al ingresar con ella, se le solicitar� que cambie la misma."+
						" Muchas gracias por utilizar Sistema Hermes, le deseamos un gran d�a.";
				try{
					Util.enviarMail(user.getMail(), asunto, mensaje);
					reestablecerPassword(user, passwordNueva);
					Util.addMessage("E-mail enviado", "Se ha enviado el mail con �xito con la informaci�n para reestablecer su cuenta.");
					Util.cerrarDialog("dlgReestablecerPassword");
				} catch (Exception e){
					Util.addMessage("Error", "Error al enviar el mail para reestablecer la cuenta, disculpe las molestias ocacionadas.");			
				}
			} else{
				Util.addMessage("Usuario no encontrado", "Error al recuperar su Usuario a partir del dato ingresado.");
			}
		} catch (Exception e){
			Util.addMessage("Usuario no encontrado", "Error al recuperar su Usuario a partir del dato ingresado.");
		}
	}

	public void reestablecerPassword(Usuario user, String passwordNueva) {
		user.setPassword(passwordNueva);
		user.setReestablecerPass(true);
		guardarUsuario(user);
	}
	
	public void setUsuario(Usuario usuario){
		this.usuario=usuario;
	}
	
	public Usuario getUsuario(){
		return this.usuario;
	}
	
	public boolean reestablecioPassword(String user) {
		setUsuario((_repositorio.findByUsuario(user)).get(0));
		return usuario.getReestablecerPassword();
	}

	public void guardarUsuarioReestablecido(String nombreUsuario, String nvaPassword) {
		Usuario user = getUsuario();
		if((user.getUsuario()).equals(nombreUsuario)){
			user.setPassword(nvaPassword);
			user.setReestablecerPass(false);
			guardarUsuario(user);
		}
	}

	public void guardarUsuario(Usuario user) {
		_repositorio.save(user);
	}
}
