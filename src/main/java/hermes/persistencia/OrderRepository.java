package hermes.persistencia;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.Pedido;

@Repository
public interface OrderRepository extends PagingAndSortingRepository<Pedido, Long>{
	public List<Pedido> findByFechaPedido(Date fechaPedido);
	public List<Pedido> findByFechaPedidoAndSucursalNombre(Date fechaPedido, String sucursal);
	public List<Pedido> findBySucursalNombre(String sucursal);
	public List<Pedido> findByDetalleNombreProducto(String nombreProducto);
	public List<Pedido> findByDetalleNombreProductoAndDetalleNombreTipoProducto(String nombreProducto, String nombreTipoProducto);
	public List<Pedido> findByDetalleNombreProductoAndDetalleNombreTipoProductoAndEstadoNombreOrEstadoNombre(String nombreProducto, String nombreTipoProducto,String estado1, String estado2);
	public List<Pedido> findByEstadoNombre(String nombre);
	public List<Pedido> findByFechaPedidoBetween(Date fechaDesde, Date fechaHasta);
	public List<Pedido> findByEstadoNombreOrEstadoNombre(String estado1, String estado2);
}
