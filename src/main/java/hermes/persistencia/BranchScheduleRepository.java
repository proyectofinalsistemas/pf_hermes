package hermes.persistencia;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.CronogramaSucursal;

@Repository
public interface BranchScheduleRepository extends PagingAndSortingRepository<CronogramaSucursal,Integer> {

}
