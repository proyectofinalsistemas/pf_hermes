package hermes.persistencia;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.EstadoPedido;

@Repository
public interface OrderStatusRepository  extends PagingAndSortingRepository<EstadoPedido, Integer>{
	public List<EstadoPedido> findByNombreIgnoreCase(String nombre);
}
