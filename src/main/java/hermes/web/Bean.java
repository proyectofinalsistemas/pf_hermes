package hermes.web;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import hermes.dominio.EstadoPedido;
import hermes.dominio.Pedido;
import hermes.dominio.Producto;
import hermes.logica.ServicioPedido;



public class Bean {
	private ServicioPedido servicio;
	private Pedido pedidosHistoricosSeleccionado;
	private boolean noHaySeleccionado;
	private EstadoPedido estado;
	
	
	 
	 public void addMessage(String summary, String detail) {
	        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
	        FacesContext.getCurrentInstance().addMessage(null, message);
	    }
}
