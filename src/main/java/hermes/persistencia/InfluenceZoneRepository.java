package hermes.persistencia;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hermes.dominio.ZonaInfluencia;

@Repository
public interface InfluenceZoneRepository extends PagingAndSortingRepository<ZonaInfluencia, Integer> {

}
