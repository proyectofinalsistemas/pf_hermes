package hermes.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data @NoArgsConstructor(access=AccessLevel.PRIVATE) @RequiredArgsConstructor
@Entity
public class TipoEntrega {

	@Id @GeneratedValue 
	private Integer id;
	@NotNull @NonNull
	private String nombre;
	private String descripcion;
	private boolean habilitado;
	
	@Override
	public String toString() {
		return this.nombre;
	}
	
}
