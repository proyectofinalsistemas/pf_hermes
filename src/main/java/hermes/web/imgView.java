package hermes.web;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

import Utilidades.Util;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@Scope("view")
public class imgView implements Serializable {
	private @Setter @Getter UploadedFile imagenFile;
	private @Setter @Getter String imagen;
	private byte[] imagenBit;
	
	public void subirArchivo(){

		int d=2;
		System.out.println(imagenFile.getFileName());
  }
	
	public void obtenerUrlImagen() {
		String url="";
		String formatoImagen = imagenFile.getFileName().toLowerCase();
		int cortar = formatoImagen.indexOf(".");
      formatoImagen = formatoImagen.substring(cortar, formatoImagen.length());
      
      if ((formatoImagen.equals(".jpg") || formatoImagen.equals(".jpeg"))) {
          try {
              imagenBit = imagenFile.getContents();
              url = Util.guardarFichero(imagenBit, imagenFile.getFileName());
              setImagen(url);
          } catch (Exception e){
          	
          	
          }
		
      } else {
			
		}
      
      
	}
}
